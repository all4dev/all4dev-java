package com.esgi.all4devjava.exception;

public class BadRequestException extends A4DException {
    public BadRequestException() {
        super(400);
    }
}
