package com.esgi.all4devjava.exception;

public class A4DException extends Exception {
    private int httpCode;

    public A4DException(int httpCode) {
        this.httpCode = httpCode;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public void setHttpCode(int httpCode) {
        this.httpCode = httpCode;
    }
}
