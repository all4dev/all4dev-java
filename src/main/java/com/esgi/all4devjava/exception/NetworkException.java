package com.esgi.all4devjava.exception;

public class NetworkException extends A4DException {
    public NetworkException() {
        super(-1);
    }
}
