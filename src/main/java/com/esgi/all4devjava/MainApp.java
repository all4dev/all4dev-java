package com.esgi.all4devjava;

import com.esgi.all4devjava.controller.LoginController;
import com.esgi.all4devjava.imageloader.ImageLoader;
import com.esgi.all4devjava.util.Util;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.Properties;

public class MainApp extends Application {

    private Stage primaryStage;
    private Pane rootLayout;

    public MainApp() {
    }

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("All 4 Dev");
        primaryStage.getIcons().add(new Image(MainApp.class.getResourceAsStream("/image/logo.png")));

        ImageLoader.init();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Stop from Main");
                ImageLoader.interrupted();
            }
        });
        initRootLayout();
    }

    public void initRootLayout() {
        try {
            //Todo: Setup Login & Select Project View
            LoginController loginController = Util.loadController(primaryStage, LoginController.class);
            loginController.show();
            /*primaryStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>() {
                public void handle(WindowEvent event) {
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setTitle("Voulez vous fermer ?");
                    alert.setContentText("Oui ?");
                    Optional<ButtonType> click = alert.showAndWait();
                    if (click.get() == ButtonType.CANCEL) {
                        event.consume();
                    }
                }
            });*/

            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    Platform.exit();
                    System.exit(0);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}