package com.esgi.all4devjava.controller;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.dialog.CreateProjectDialog;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.list.cell.ProjectListCell;
import com.esgi.all4devjava.object.*;
import com.esgi.all4devjava.util.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import retrofit2.Call;
import retrofit2.Response;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LoginController extends Controller {

    public static URL layoutURL = Util.getResourceLayout("LoginLayout");
    @FXML
    ProgressIndicator loader;


    //Block Login Form
    @FXML
    Node loginBlockLogin;

    @FXML
    TextField loginUsernameField;

    @FXML
    PasswordField loginPasswordField;

    @FXML
    Button loginBtnLogin;



    //Block Select Project
    @FXML
    Node loginBlockProject;

    @FXML
    ListView<Project> loginListProject;

    @FXML
    Label loginLblNoProject;

    @FXML
    Button loginBtnAddProject;

    @FXML
    Label loginProjectName;

    @FXML
    TextField loginProjectPathDir;

    @FXML
    Button loginProjectBtnChooseDir;

    @FXML
    Button loginProjectBtnValidate;


    //Block Key
    @FXML
    VBox loginBlockKey;

    @FXML
    TextField loginPrivateKeyPathDir;

    @FXML
    Button loginPrivateKeyBtnChooseDir;

    @FXML
    TextField loginPublicKeyPathDir;

    @FXML
    Button loginPublicKeyBtnChooseDir;

    @FXML
    Button loginPublicKeyBtnConfirm;


    public List<Project> projects = new ArrayList<Project>();
    private File projectDir;

    public String authKey;
    public User user;

    public CreateProjectDialog createProjectDialog;

    private Settings settings;

    public ApiManager apiManager;


    @Override
    public void initialize() {

        try {
            File folder = StorageManager.getA4DDir();
            if (!folder.exists()) {
                folder.mkdirs();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        try {
            createProjectDialog = Util.loadDialog(getPrimaryStage(), CreateProjectDialog.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        loginBtnLogin.setOnAction(event -> onClickLogin());
        loginListProject.setItems(FXCollections.observableArrayList());

        loginListProject.setCellFactory(
                listView -> new ProjectListCell() {
                    @Override
                    public void onClick(Project project) {
                        onProjectSelect(project);
                    }
                }
        );
        EventHandler<ActionEvent> actionNoProject = new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                //Message: "Please select a project"
            }
        };
        loginProjectBtnChooseDir.setOnAction(actionNoProject);
        loginProjectBtnValidate.setOnAction(actionNoProject);
        loginBtnAddProject.setOnAction(event -> {
            createProjectDialog.clearAndShow(this);
        });
    }

    private void onClickLogin() {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    loginBlockLogin.setVisible(false);
                    loader.setVisible(true);
                }

                @Override
                public void onSuccess() {
                    if (settings == null)
                        settings = new Settings();
                    if (settings.getPrivateKeyPath() == null || settings.getPublicKeyPath() == null)
                        initializeKeyView();
                    else
                        initializeProjectView();
                }

                @Override
                public void onError(A4DException e) {
                    e.printStackTrace();
                    loginBlockLogin.setVisible(true);
                }

                @Override
                public void always() {
                    loader.setVisible(false);
                }
            };
            apiManager.execute(new ApiRequest<AuthResponse>() {

                @Override
                public Call<AuthResponse> call(ApiService service) {
                    return service.auth(loginUsernameField.getText(), loginPasswordField.getText());
                }

                @Override
                public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                    System.out.println(response.body().getKey());
                    authKey = "Bearer " + response.body().getKey();
                    SessionData.getInstance().setAuthToken(authKey);
                }
            }, new ApiRequest<List<Project>>() {
                @Override
                public Call<List<Project>> call(ApiService service) {
                    return service.getProjects(authKey);
                }

                @Override
                public void onResponse(Call<List<Project>> call, Response<List<Project>> response) {
                    projects = response.body();
                    Collections.sort(projects, new DateTimeComparator<Project>(DateTimeComparator.Sort.DESC));
                }
            }, new ApiRequest<User>() {
                @Override
                public Call<User> call(ApiService service) {
                    return service.getMe(authKey);
                }

                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    user = response.body();
                    SessionData.getInstance().setCurrentUser(user);
                    try {
                        settings = Settings.load();
                    } catch (IOException e) {
                        settings = null;
                    }
                }
            });
        }
    }


    //Projects
    private void initializeProjectView() {
        loginBlockLogin.setVisible(false);
        loginBlockKey.setVisible(false);
        loginBlockProject.setVisible(true);
        updateListViewProject();
    }

    public void updateListViewProject() {
        if (projects.size() == 0) {
            loginListProject.setVisible(false);
            loginLblNoProject.setVisible(true);
        } else {
            loginListProject.setVisible(true);
            loginLblNoProject.setVisible(false);
            loginListProject.getItems().setAll(projects);
        }
    }

    private void onProjectSelect(Project project) {
        loginProjectName.setText(project.getName());
        selectProjectDir(StorageManager.getProjectDir(project.getName()));
        loginProjectBtnChooseDir.setOnAction((e) -> {
                    DirectoryChooser directoryChooser = new DirectoryChooser();
                    if (projectDir.exists())
                        directoryChooser.setInitialDirectory(projectDir);

                    File selectedDirectory = directoryChooser.showDialog(loginBlockLogin.getScene().getWindow());
                    if (selectedDirectory != null) {
                        selectProjectDir(selectedDirectory);
                    }
                }
        );
        loginProjectBtnValidate.setOnAction((e) -> {
            try {
                if (!projectDir.exists() && !projectDir.mkdirs()) {
                    System.out.println("Mkdir failed");
                } else {
                    onClickConfirmProject(project);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
    }

    private void selectProjectDir(File dir) {
        projectDir = dir;
        loginProjectPathDir.setText(projectDir.getAbsolutePath());
    }

    private void onClickConfirmProject(Project project) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {

                @Override
                public void onPreExec() {
                    loader.setVisible(true);
                    loginBlockProject.setVisible(false);
                }

                @Override
                public void onSuccess() {
                    try {
                        RootController controller = Util.loadController(getPrimaryStage(), RootController.class);
                        SessionData.getInstance().setCurrentProject(project);
                        controller.initValue(authKey, user, project, settings, projectDir);
                        controller.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(A4DException e) {
                    loader.setVisible(false);
                    loginBlockProject.setVisible(true);
                }

                @Override
                public void always() {

                }
            };
            apiManager.execute(new ApiRequest<List<Member>>() {
                @Override
                public Call<List<Member>> call(ApiService service) {
                    return service.getProjectMembers(authKey, project.getLeaderUsername(), project.getName());
                }

                @Override
                public void onResponse(Call<List<Member>> call, Response<List<Member>> response) {
                    List<Member> members = new ArrayList<Member>(response.body().size());
                    members.addAll(response.body());
                    project.setMembers(members);
                }
            }, new ApiRequest<List<Version>>() {
                @Override
                public Call<List<Version>> call(ApiService service) {
                    return service.getProjectVersions(authKey, project.getLeaderUsername(), project.getName());
                }

                @Override
                public void onResponse(Call<List<Version>> call, Response<List<Version>> response) {
                    List<String> versions = new ArrayList<String>(response.body().size());
                    for (Version v : response.body()) {
                        versions.add(v.getVersion());
                    }
                    project.setVersions(versions);
                }
            }, new ApiRequest<List<Task>>() {
                @Override
                public Call<List<Task>> call(ApiService service) {
                    return service.getTasks(authKey, project.getLeaderUsername(), project.getName());
                }

                @Override
                public void onResponse(Call<List<Task>> call, Response<List<Task>> response) {
                    ArrayList<Task> tasks = (ArrayList<Task>) response.body();
                    SessionData.getInstance().setCurrentTasks(tasks);
                    project.setTasks(response.body());
                }
            });
        }
    }


    //Keys
    private void initializeKeyView() {
        loginBlockLogin.setVisible(false);
        loginBlockProject.setVisible(false);
        loginBlockKey.setVisible(true);
        File privateKey = new File(System.getProperty("user.home") + File.separator + ".ssh" + File.separator + "id_rsa");
        File publicKey = new File(System.getProperty("user.home") + File.separator + ".ssh" + File.separator + "id_rsa.pub");
        if (privateKey.exists()) {
            selectPrivateKey(privateKey);
        }
        if (publicKey.exists()) {
            selectPublicKey(publicKey);
        }
        loginPrivateKeyBtnChooseDir.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            File file = new File(settings.getPrivateKeyPath());
            fileChooser.setInitialDirectory(file.getParentFile());
            fileChooser.setInitialFileName(file.getAbsolutePath());
            File selectedFile = fileChooser.showOpenDialog(loginBlockLogin.getScene().getWindow());
            if (selectedFile != null) {
                selectPrivateKey(selectedFile);
            }
        });
        loginPublicKeyBtnChooseDir.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            File file = new File(settings.getPublicKeyPath());
            fileChooser.setInitialDirectory(file.getParentFile());
            fileChooser.setInitialFileName(file.getName());
            File selectedFile = fileChooser.showOpenDialog(loginBlockLogin.getScene().getWindow());
            if (selectedFile != null) {
                selectPublicKey(selectedFile);
            }
        });
        loginPublicKeyBtnConfirm.setOnAction(event -> onClickSaveKey());
    }

    private void selectPrivateKey(File file) {
        settings.setPrivateKeyPath(file.getAbsolutePath());
        loginPrivateKeyPathDir.setText(file.getAbsolutePath());
    }

    private void selectPublicKey(File file) {
        settings.setPublicKeyPath(file.getAbsolutePath());
        loginPublicKeyPathDir.setText(file.getAbsolutePath());
    }

    private void onClickSaveKey() {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    loader.setVisible(true);
                    loginBlockKey.setVisible(false);
                }

                @Override
                public void onSuccess() {
                    try {
                        System.out.println("here");
                        settings.save();
                        initializeProjectView();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(A4DException e) {
                    if (e.getHttpCode() == 409) {
                        System.out.println("here");
                        onSuccess();
                    } else {
                        loginBlockKey.setVisible(true);
                    }
                }

                @Override
                public void always() {
                    loader.setVisible(false);
                }
            };
            apiManager.execute(new ApiRequest<PublicKey>() {
                @Override
                public Call<PublicKey> call(ApiService service) {
                    String keyName = System.getProperty("user.name") + " " + System.getProperty("os.name");
                    keyName = keyName.replaceAll(" ", "_");
                    return service.addPublicKey(authKey,
                            user.getUsername(),
                            new PublicKey(keyName, getContentFile(settings.getPublicKeyPath()))
                    );
                }

                @Override
                public void onResponse(Call<PublicKey> call, Response<PublicKey> response) {
                }
            });
        }
    }

    private String getContentFile(String file) {
        FileReader fileReader = null;
        BufferedReader bufferedReader = null;
        try {
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            String content = "";
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content += line;
            }
            return content;
        } catch (Exception e) {
            return null;

        } finally {
            try {
                if (bufferedReader != null)
                    bufferedReader.close();
                if (fileReader != null)
                    fileReader.close();
            } catch (IOException e) {
            }
        }
    }
}
