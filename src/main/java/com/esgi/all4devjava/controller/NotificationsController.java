package com.esgi.all4devjava.controller;

import com.esgi.all4devjava.object.Notification;
import com.esgi.all4devjava.object.SessionData;
import com.esgi.all4devjava.util.Util;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Region;

import java.net.URL;


public class NotificationsController extends FragmentController {
    public static URL layoutURL = Util.getResourceLayout("NotificationsLayout");

    @FXML
    TableColumn typeColumn;
    @FXML
    TableColumn messageColumn;
    @FXML
    TableView notificationTable;

    private ObservableList<Notification> notifications = FXCollections.observableArrayList();
    @Override
    public void initialize() {
        notifications.setAll(SessionData.getInstance().getCurrentNotifications());
        typeColumn.setCellValueFactory(new PropertyValueFactory<Notification,String>("type"));
        messageColumn.setCellValueFactory(new PropertyValueFactory<Notification,String>("body"));
        notificationTable.setItems(notifications);

    }
    public void notify(String authToken){

    }

    @Override
    public Region getRoot() {
        return super.getRoot();
    }

    @FXML
    public void read(){
        if(notificationTable.getSelectionModel().getSelectedItem() != null){
            SessionData.getInstance().removeNotification(notificationTable.getSelectionModel().getSelectedIndex());
            notifications.remove(notificationTable.getSelectionModel().getSelectedIndex());
            notificationTable.refresh();
        }
    }
}
