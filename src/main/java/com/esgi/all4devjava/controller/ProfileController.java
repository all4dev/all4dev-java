package com.esgi.all4devjava.controller;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.object.SessionData;
import com.esgi.all4devjava.object.User;
import com.esgi.all4devjava.spamapi.SpamRequestApi;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.net.URL;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by tayeb on 27/06/2016.
 */
public class ProfileController extends FragmentController {
    public static URL layoutURL = Util.getResourceLayout("ProfileView");

    String authToken;
    User user;

    @FXML
    TextField tfPseudo;

    @FXML
    TextField tfPrenom;

    @FXML
    TextField tfNom;

    @FXML
    TextField tfEmail;

    @FXML
    Button btnSubmit;

    @FXML
    ImageView ivProfil;

    @Override
    public void initialize() {
    }

    @FXML
    public void fEnvoie() throws IOException {
            onUpdateProfile();
    }

    public void notify(String authToken, User user) {

        tfPseudo.setText(user.getUsername());
        tfEmail.setText(user.getEmail());
        tfNom.setText(user.getLastname());
        tfPrenom.setText(user.getFirstname());
        ivProfil.toString();
    }

    public void onUpdateProfile() {
            new ApiManager() {
                @Override
                public void onPreExec() {
                }

                @Override
                public void onSuccess() {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Profil");
                    alert.setHeaderText(null);
                    alert.setContentText("Enregistrement effectué avec succès");

                    alert.showAndWait();
                }

                @Override
                public void onError(A4DException e) {
                    e.printStackTrace();
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Echec");
                    alert.setHeaderText(null);
                    alert.setContentText("l'enregistrement a échouer. Veuillez contactez l'administrateur");

                    alert.showAndWait();
                }

                @Override
                public void always() {
                }
            }.execute(new ApiRequest<User>() {
                @Override
                public Call<User> call(ApiService service) {
                    return service.putUser(authToken,
                            tfPseudo.getText(),
                            tfPrenom.getText().toString(),
                            tfNom.getText().toString(),
                            tfEmail.getText().toString()
                    );
                }

                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    user = response.body();
                }
            });
        }
}
