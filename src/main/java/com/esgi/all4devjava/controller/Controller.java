package com.esgi.all4devjava.controller;

import com.esgi.all4devjava.spamapi.SpamRequestApi;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.net.URL;

public abstract class Controller {
    private Region root;
    private Stage primaryStage;
    public static URL layoutURL = null;
    private ContextMenu contextMenu;

    @FXML
    public abstract void initialize();

    public void show() {
        Scene scene = new Scene(getRoot());
        scene.getStylesheets().add("/style/style.css");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void setSubTitle(String subTitle) {
        primaryStage.setTitle("All 4 Dev - " + subTitle);
    }

    public Region getRoot() {
        return root;
    }

    public void setRoot(Region root) {
        this.root = root;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void setContextItem(MenuItem... items) {
        contextMenu = new ContextMenu(items);
        getPrimaryStage().getScene().setOnMouseClicked(event -> {
            closeContext();
        });
        for (MenuItem item : items) {
            item.setOnAction(event -> onContextItemClick(item));
        }
    }

    public void showContext(double x, double y) {
        if (contextMenu != null)
            contextMenu.show(getPrimaryStage(), x, y);
    }

    public void closeContext() {
        if (contextMenu != null) {
            contextMenu.hide();
            onContextClose();
        }
    }

    public void onContextItemClick(MenuItem item) {

    }

    public void onContextClose() {

    }
}
