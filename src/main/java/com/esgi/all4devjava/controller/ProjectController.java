package com.esgi.all4devjava.controller;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.dialog.*;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.list.cell.MembersProjectAccessTileGridCell;
import com.esgi.all4devjava.menu.UserMenuItem;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.Project;
import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.object.Version;
import com.esgi.all4devjava.util.*;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import org.controlsfx.control.GridCell;
import org.controlsfx.control.GridView;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.TagCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.merge.MergeStrategy;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTag;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.PushResult;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProjectController extends FragmentController {
    public static URL layoutURL = Util.getResourceLayout("ProjectView");

    @FXML
    Label projectTextTitle;

    @FXML
    Label projectTextLeader;

    @FXML
    Label projectTextDesc;

    @FXML
    Label projectVersionTitle;


    @FXML
    public GridView<Member> projectGridviewMember;

    @FXML
    Button projectBtnAddMember;
    Member rightClickMember;

    private String authKey;
    public RootController rootController;
    public AddProjectMemberDialog addDialog;
    public RemoveProjectMemberDialog removeDialog;
    public UpdateProjectMemberDialog updateDialog;
    public VersionProjectMemberDialog versionDialog;
    Project project;
    public ApiManager apiManager;
    public List<Version> versions = new ArrayList<Version>();



    @FXML
    public void fOpen() throws IOException {
        versionDialog.notifyAndShow(this);
    }

    @FXML
    public void onNextVersion() {
        Collections.sort(project.getVersions(), new VersionComparator());
        int currentVersionIndex = project.getVersions().indexOf(project.getCurrentVersion());
        String nextVersion = project.getVersions().get(currentVersionIndex + 1);
        if (currentVersionIndex != -1 && project.getVersions().size() -1 != currentVersionIndex) {
            List<Task> blockedTasks = new ArrayList<>();
            for (Task task : project.getTasks())
                if (task.getVersion().equalsIgnoreCase(nextVersion) && task.getStatus() != Task.Status.MERGED)
                    blockedTasks.add(task);

            if (blockedTasks.size() == 0) {
                System.out.println("OK " + nextVersion);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Erreur");
                alert.setHeaderText("Impossible");
                StringBuffer s = new StringBuffer("Cette version est définie dans les taches :");
                for (Task task : blockedTasks) {
                    s.append("\n" + task.getName() + " -> " + task.getStatus());
                }
                alert.getDialogPane().setContent(new Label(s.toString()));

                alert.showAndWait();
                return;
            }
        }






        GitManager gitManager = rootController.gitManager;

        RevWalk revWalk = new RevWalk(gitManager.getGit().getRepository());
        RevCommit masterHead = null;
        RevCommit versionHead = null;
        boolean alreadyMerged = false;
        try {
            versionHead = revWalk.parseCommit(gitManager.getGit().getRepository().resolve("refs/heads/version/" + nextVersion));
            masterHead = revWalk.parseCommit(gitManager.getGit().getRepository().resolve("refs/heads/master"));
            if (revWalk.isMergedInto(versionHead, masterHead)) {
                alreadyMerged = true;
            }

            if (alreadyMerged) {
                System.out.println("Already Merged");
                nextVersion(nextVersion);
                revWalk.close();
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
            revWalk.close();
            return;
        }

        gitManager.put(new GitManagerCommands(
                new GitManagerCommand<Ref>(
                        "Checkout to master",
                        gitManager.getGit()
                                .checkout()
                                .setName("master")
                                .setCreateBranch(false)
                ),
                new GitManagerCommand<MergeResult>(
                        "Merging branch version/" + nextVersion + " into master",
                        gitManager.getGit()
                                .merge()
                                .setStrategy(MergeStrategy.RESOLVE)
                                .include(versionHead)
                ),
                new GitManagerCommand<Ref>(
                        "Tag create",
                        gitManager.getGit()
                                .tag()
                                .setName("v" + nextVersion)
                ),
                new GitManagerCommand<Iterable<PushResult>>(
                        "Push to origin",
                        gitManager.getGit().push()
                                .setRemote("origin")
                )) {
            @Override
            public void onError(GitAPIException e) {
                revWalk.close();
                e.printStackTrace();
                rootController.errorDialog.notifyAndShow(
                        "Impossible de merge !\n" +
                                "git checkout master\n" +
                                "git merge version/" + nextVersion + "\n" +
                                "Faite les modifications si vous en avez. Puis re cliquez sur ce button"
                );
            }

            @Override
            public void onFinish() {
                revWalk.close();
                rootController.timelinePull.play();
                nextVersion(nextVersion);
            }
        });

    }

    private void updateProjectVersion() {
        projectVersionTitle.setText("Numéro de version : " + project.getCurrentVersion());
    }

    private void nextVersion(String nextVersion) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                }

                @Override
                public void onSuccess() {
                    updateProjectVersion();
                }

                @Override
                public void onError(A4DException e) {
                }

                @Override
                public void always() {
                }
            };
            apiManager.execute(new ApiRequest<Version>() {
                @Override
                public Call<Version> call(ApiService service) {
                    return service.patchProjectVersion(
                            rootController.authKey,
                            rootController.project.getLeaderUsername(),
                            rootController.project.getName(),
                            nextVersion);
                }

                @Override
                public void onResponse(Call<Version> call, Response<Version> response) {
                    project.setCurrentVersion(response.body().getVersion());
                }
            });
        }

    }



    @Override
    public void initialize() {

        Callback<GridView<Member>, GridCell<Member>> cellFactory = param -> new MembersProjectAccessTileGridCell() {
            @Override
            public void onClick(Member member) {
                rootController.menuManager.forceLoad(new UserMenuItem(authKey, member));
            }

            @Override
            public void onRightClick(Member member, MouseEvent mouseEvent) {
                if (member.getAccess() != Member.Access.ADMIN) {
                    rightClickMember = member;
                    showContext(mouseEvent.getScreenX(), mouseEvent.getScreenY());
                }
            }
        };


        projectGridviewMember.setCellHeight(120);
        projectGridviewMember.setCellWidth(80);
        projectGridviewMember.setCellFactory(cellFactory);
        projectGridviewMember.setItems(FXCollections.observableArrayList());

        try {addDialog = Util.loadDialog(getPrimaryStage(), AddProjectMemberDialog.class);}
        catch (Exception e) {e.printStackTrace();}

        try {removeDialog = Util.loadDialog(getPrimaryStage(), RemoveProjectMemberDialog.class);}
        catch (Exception e) {e.printStackTrace();}

        try {updateDialog = Util.loadDialog(getPrimaryStage(), UpdateProjectMemberDialog.class);}
        catch (Exception e) {e.printStackTrace();}

        try {versionDialog = Util.loadDialog(getPrimaryStage(), VersionProjectMemberDialog.class);}
        catch (Exception e) {e.printStackTrace();}

        projectBtnAddMember.setOnAction(event -> {
            addDialog.notifyAndShow(this);
        });

        setContextItem(Util.ContextItem.UPDATE, Util.ContextItem.DELETE);
    }

    @Override
    public void onContextItemClick(MenuItem item) {
        if (item == Util.ContextItem.UPDATE) {
            System.out.println("updateTask " + rightClickMember.getUsername());
            updateDialog.notifyAndShow(this, rightClickMember);
        } else if (item == Util.ContextItem.DELETE) {
            System.out.println("Delete " + rightClickMember.getUsername());
            removeDialog.notifyAndShow(this, rightClickMember);
        }
    }

    @Override
    public void onContextClose() {
        rightClickMember = null;
    }

    public void notify(String authKey, Project project, RootController rootController) {
        this.project = project;
        this.authKey = authKey;
        this.rootController = rootController;
        projectTextTitle.setText(project.getName());
        projectTextLeader.setText(project.getLeaderUsername());
        updateProjectVersion();
        projectTextDesc.setText(project.getDescription());
        projectGridviewMember.getItems().setAll(project.getMembers());
    }


    public void updateMemberListView() {
        projectGridviewMember.getItems().setAll(rootController.project.getMembers());
    }
}
