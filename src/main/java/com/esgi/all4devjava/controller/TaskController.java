package com.esgi.all4devjava.controller;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.dialog.AddTaskMemberDialog;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.list.cell.MembersProjectTileListCell;
import com.esgi.all4devjava.list.cell.TaskListCell;
import com.esgi.all4devjava.list.data.ContentCell;
import com.esgi.all4devjava.list.data.TaskCommentContentCell;
import com.esgi.all4devjava.list.data.TaskCommitContentCell;
import com.esgi.all4devjava.list.data.TaskStatusContentCell;
import com.esgi.all4devjava.object.*;
import com.esgi.all4devjava.util.*;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.controlsfx.control.SegmentedButton;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.merge.MergeStrategy;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.transport.FetchResult;
import org.eclipse.jgit.transport.PushResult;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskController extends FragmentController {
    public static URL layoutURL = Util.getResourceLayout("TaskView");

    @FXML
    Button taskBtnReload;


    @FXML
    Label taskTextTitle;

    @FXML
    Label taskTextVersion;

    @FXML
    Label taskTextAuthor;

    @FXML
    Label taskLabelDesc;

    @FXML
    ListView<TaskMember> taskListMembers;

    @FXML
    StackPane taskStackStatus;

    @FXML
    StackPane taskListLoader;
    SegmentedButton segBtnStatus;

    @FXML
    Button taskBtnAddMember;

    @FXML
    Label taskTextNoMembers;

    @FXML
    VBox taskVboxContent;

    @FXML
    TextArea taskTextComment;

    @FXML
    StackPane taskLoaderComment;

    @FXML
    Button taskBtnComment;

    private boolean firstAppear = true;

    public RootController rootController;
    public ApiManager apiManager;
    public String authKey;
    public Project project;
    public Task task;

    private AddTaskMemberDialog addTaskMemberDialog;
    TaskListCell taskContentListCell;


    //Temp data
    private Comment addedComment;
    private Member removingMember;
    private TaskStatus newTaskStatus;


    @Override
    public void initialize() {
        taskBtnComment.setOnAction(event -> onComment());
        taskListMembers.setItems(FXCollections.observableArrayList());
        taskBtnAddMember.setOnAction(event -> {
            List<Member> members = new ArrayList<Member>(project.getMembers().size());
            for(Member member : project.getMembers()) {
                if (member.getAccess() != Member.Access.VIEWER)
                    members.add(member);
            }
            members.removeAll(task.getMembers());
            if (members.size() != 0) {
                if (addTaskMemberDialog != null)
                    addTaskMemberDialog.notifyAndShow(this, members);
            } else {
                System.out.println("Aucun membre a ajouter.");
            }
        });

        try {
            addTaskMemberDialog = Util.loadDialog(getPrimaryStage(), AddTaskMemberDialog.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContextItem(Util.ContextItem.DELETE);
        taskListMembers.setCellFactory(
                listView -> (ListCell<TaskMember>)(ListCell<?>) new MembersProjectTileListCell() {
                    @Override
                    public void onRightClick(Member member, MouseEvent mouseEvent) {
                        System.out.println("right click " + member.toString());
                        removingMember = member;
                        showContext(mouseEvent.getScreenX(), mouseEvent.getScreenY());
                    }
                }
        );

        segBtnStatus = new SegmentedButton(Util.getToggleFromEnum(Task.Status.class));
        taskStackStatus.getChildren().add(segBtnStatus);
    }

    public void notify(RootController rootController, String authKey, Project project, Task task) {
        this.rootController = rootController;
        this.authKey = authKey;
        this.project = project;
        this.task = task;
        if (firstAppear) {
            pullTaskInformation();
            firstAppear = false;
        }

        taskBtnReload.setOnAction(event -> pullTaskInformation());
        taskContentListCell = new TaskListCell();
    }

    public void updateTask() {
        taskTextTitle.setText(task.getName());
        taskTextAuthor.setText(task.getAuthorName());
        taskTextVersion.setText(task.getVersion());
        taskLabelDesc.setText(task.getBody());
        updateStatusButtons();
    }

    @Override
    public void onContextItemClick(MenuItem item) {
        onRemoveMember(removingMember);
    }

    @Override
    public void onContextClose() {
        removingMember = null;
    }

    private void pullTaskInformation() {
        apiManager = new ApiManager() {
            @Override
            public void onPreExec() {

            }

            @Override
            public void onSuccess() {
                /*taskListView.getItems().clear();
                taskListView.getItems().addAll(comments);
                taskListView.getItems().addAll(commits);

                for (Node node : taskListView.getChildrenUnmodifiable()) {
                }*/
                List<DateTimeable> dateTimeables = new ArrayList<DateTimeable>(task.getComments().size() + task.getCommits().size());
                dateTimeables.addAll(task.getComments());
                dateTimeables.addAll(task.getCommits());
                dateTimeables.addAll(task.getStatuses());
                Collections.sort(dateTimeables, new DateTimeComparator<>(DateTimeComparator.Sort.ASC));
                taskVboxContent.getChildren().clear();
                for (DateTimeable dateTimeable : dateTimeables) {
                    addContent(dateTimeable);
                }
                updateTask();
                updateListMembersView();
            }

            @Override
            public void onError(A4DException e) {

            }

            @Override
            public void always() {

            }
        };
        apiManager.execute(new ApiRequest<Task>() {
            @Override
            public Call<Task> call(ApiService service) {
                return service.getTask(authKey, project.getLeaderUsername(), project.getName(), task.getId());
            }

            @Override
            public void onResponse(Call<Task> call, Response<Task> response) {
                task.setStatus(response.body().getStatus());
                task.setBody(response.body().getBody());
            }
        }, new ApiRequest<List<Comment>>() {
            @Override
            public Call<List<Comment>> call(ApiService service) {
                return service.getTaskComments(authKey, project.getLeaderUsername(), project.getName(), task.getId());
            }

            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                task.getComments().clear();
                task.getComments().addAll(response.body());
            }
        }, new ApiRequest<List<Commit>>() {
            @Override
            public Call<List<Commit>> call(ApiService service) {
                return service.getTaskCommits(authKey, project.getLeaderUsername(), project.getName(), task.getId());
            }

            @Override
            public void onResponse(Call<List<Commit>> call, Response<List<Commit>> response) {
                task.getCommits().clear();
                task.getCommits().addAll(response.body());
            }
        }, new ApiRequest<List<TaskMember>>() {
            @Override
            public Call<List<TaskMember>> call(ApiService service) {
                return service.getTaskMembers(authKey, project.getLeaderUsername(), project.getName(), task.getId());
            }

            @Override
            public void onResponse(Call<List<TaskMember>> call, Response<List<TaskMember>> response) {
                task.getMembers().clear();
                task.getMembers().addAll(response.body());
            }
        }, new ApiRequest<List<TaskStatus>>() {
            @Override
            public Call<List<TaskStatus>> call(ApiService service) {
                return service.getTaskStatus(authKey, project.getLeaderUsername(), project.getName(), task.getId());
            }

            @Override
            public void onResponse(Call<List<TaskStatus>> call, Response<List<TaskStatus>> response) {
                task.getStatuses().clear();
                task.getStatuses().addAll(response.body());
            }
        });
    }

    private void onComment() {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    taskLoaderComment.setVisible(true);
                    taskBtnComment.setDisable(true);
                }

                @Override
                public void onSuccess() {
                    taskTextComment.clear();
                    addContent(addedComment);
                }

                @Override
                public void onError(A4DException e) {
                }

                @Override
                public void always() {
                    taskLoaderComment.setVisible(false);
                    taskBtnComment.setDisable(false);
                    addedComment = null;
                }
            };
            apiManager.execute(new ApiRequest<Comment>() {
                @Override
                public Call<Comment> call(ApiService service) {
                    return service.addTaskComment(authKey, project.getLeaderUsername(), project.getName(), task.getId(), taskTextComment.getText());
                }

                @Override
                public void onResponse(Call<Comment> call, Response<Comment> response) {
                    addedComment = response.body();
                }
            });
        }
    }

    private void onRemoveMember(Member member) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    taskListLoader.setVisible(true);
                    taskBtnAddMember.setDisable(true);
                }

                @Override
                public void onSuccess() {
                    task.getMembers().remove(removingMember);
                    updateListMembersView();
                }

                @Override
                public void onError(A4DException e) {

                }

                @Override
                public void always() {
                    taskListLoader.setVisible(false);
                    taskBtnAddMember.setDisable(false);
                    removingMember = null;
                }
            };
            apiManager.execute(new ApiRequest<Void>(){
                @Override
                public Call<Void> call(ApiService service) {
                    return service.removeTaskMember(authKey, project.getLeaderUsername(), project.getName(), task.getId(), member.getUsername());
                }

                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                }
            });
        }
    }

    private void onChangeStatus(Task.Status status) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    newTaskStatus = null;
                }

                @Override
                public void onSuccess() {
                    addContent(newTaskStatus);
                    updateStatusButtons();
                }

                @Override
                public void onError(A4DException e) {

                }

                @Override
                public void always() {
                    newTaskStatus = null;
                }
            };
            apiManager.execute(new ApiRequest<TaskStatus>() {
                @Override
                public Call<TaskStatus> call(ApiService service) {
                    return service.updateTaskStatus(authKey, project.getLeaderUsername(), project.getName(), task.getId(), status.toString());
                }

                @Override
                public void onResponse(Call<TaskStatus> call, Response<TaskStatus> response) {
                    newTaskStatus = response.body();
                    task.setLastUpdate(newTaskStatus.getCreationDate());
                    task.getStatuses().add(newTaskStatus);
                    task.setStatus(newTaskStatus.getStatus());
                }
            });
        }
    }

    private void applyGitChange(Task.Status status) {
        segBtnStatus.getToggleGroup().selectToggle(null);
        GitManager gitManager = rootController.gitManager;
        Member.Access myAccess = rootController.project.getMember(rootController.user).getAccess();

        /////////////////////////////
        //     RUNNING
        /////////////////////////////
        if (status == Task.Status.RUNNING) {
            if (myAccess == Member.Access.VIEWER)
                return;

            if (!task.getMembers().contains(rootController.user) && !(myAccess == Member.Access.MOD || myAccess == Member.Access.ADMIN)) {
                return;
            }

            rootController.timelinePull.stop();
            gitManager.put(new GitManagerCommands(
                    new GitManagerCommand<Ref>(
                            "Create task branch " + task.getRef(),
                            gitManager.getGit().checkout()
                                    .setCreateBranch(true)
                                    .setForce(true)
                                    .setName(task.getRef())
                                    .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                                    .setStartPoint("master")
                    ),
                    new GitManagerCommand<Iterable<PushResult>>(
                            "Push to origin",
                            gitManager.getGit().push()
                                    .setRemote("origin")
                    )
            ) {
                @Override
                public void onFinish() {
                    rootController.timelinePull.play();
                    onChangeStatus(status);
                }
            });







            /////////////////////////////
            //     MERGING
            /////////////////////////////
        } else if (status == Task.Status.MERGING) {
            if (!(myAccess == Member.Access.MOD || myAccess == Member.Access.ADMIN))
                return;

            rootController.timelinePull.stop();
            gitManager.put(new GitManagerCommands(
                    new GitManagerCommand<FetchResult>(
                            "Fetch from origin",
                            gitManager.getGit()
                                    .fetch()
                                    .setRemote("origin")
                    ),
                    new GitManagerCommand<Ref>(
                            "Create merging branch for " + task.getRef(),
                            gitManager.getGit()
                                    .branchCreate()
                                    .setName("merging/" + task.getRef())
                                    .setStartPoint("origin/" + task.getRef())
                                    .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                                    .setForce(true)
                    ),
                    new GitManagerCommand<Iterable<PushResult>>(
                            "Push to origin",
                            gitManager.getGit().push()
                                    .setRemote("origin")
                    )
            ) {
                @Override
                public void onError(GitAPIException e) {
                    super.onError(e);
                }

                @Override
                public void onFinish() {
                    rootController.timelinePull.play();
                    onChangeStatus(status);
                }
            });










            /////////////////////////////
            //     MERGED
            /////////////////////////////
        } else if (status == Task.Status.MERGED) {
            if (!(myAccess == Member.Access.MOD || myAccess == Member.Access.ADMIN))
                return;

            boolean alreadyMerged = false;
            boolean newBranch = false;
            RevWalk revWalk = new RevWalk(gitManager.getGit().getRepository());
            RevCommit versionHead = null;
            RevCommit mergingHead = null;
            try {
                mergingHead = revWalk.parseCommit(gitManager.getGit().getRepository().resolve("refs/heads/merging/" + task.getRef()));

                ObjectId verionsObj = gitManager.getGit().getRepository().resolve("refs/heads/version/" + task.getVersion());
                if (verionsObj == null) {
                    newBranch = true;
                } else {
                    versionHead = revWalk.parseCommit(verionsObj);
                    if (revWalk.isMergedInto(mergingHead, versionHead)) {
                        alreadyMerged = true;
                    }

                    if (alreadyMerged) {
                        System.out.println("Already Merged");
                        onChangeStatus(status);
                        revWalk.close();
                        return;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                revWalk.close();
                return;
            }

            GitManagerCommand gitCommand = null;
            GitManagerCommand gitCommand2 = null;
            if (newBranch) {
                gitCommand = new GitManagerCommand<Ref>(
                        "Create version branch for " + task.getVersion(),
                        gitManager.getGit().checkout()
                                .setCreateBranch(true)
                                .setForce(true)
                                .setName("version/" + task.getVersion())
                                .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.SET_UPSTREAM)
                                .setStartPoint("master")
                );
            } else {
                gitCommand = new GitManagerCommand<Ref>(
                        "Checkout to version branch " + task.getVersion(),
                        gitManager.getGit().checkout()
                                .setName("version/" + task.getVersion())
                                .setCreateBranch(false)
                );
                gitCommand2 = new GitManagerCommand<PullResult>(
                        "Pull from origin",
                        gitManager.getGit().pull()
                                .setRemote("origin")
                );
            }



            gitManager.put(new GitManagerCommands(
                    gitCommand,
                    gitCommand2,
                    new GitManagerCommand<MergeResult>(
                            "Merging branch merging/" + task.getRef() + " into version/" + task.getVersion(),
                            gitManager.getGit()
                                    .merge()
                                    .setStrategy(MergeStrategy.RESOLVE)
                                    .include(mergingHead)
                    ),
                    new GitManagerCommand<Iterable<PushResult>>(
                            "Push to origin",
                            gitManager.getGit().push()
                                    .setRemote("origin")
                    )) {
                @Override
                public void onError(GitAPIException e) {
                    revWalk.close();
                    e.printStackTrace();
                    rootController.errorDialog.notifyAndShow(
                            "Impossible de merge !\n" +
                                    "git checkout version/"+ task.getVersion() + "\n" +
                                    "git merge merging/" + task.getRef() + "\n" +
                                    "Faite les modifications si vous en avez. Puis re cliquez sur ce button"
                    );
                }

                @Override
                public void onFinish() {
                    revWalk.close();
                    rootController.timelinePull.play();
                    onChangeStatus(status);
                }
            });
        } else {
            onChangeStatus(status);
        }
    }

    private void addContent(DateTimeable dateTimeable) {
        ContentCell contentCell = taskContentListCell.getContentCell(dateTimeable);
        if (contentCell != null) {
            Node root = contentCell.getRoot();
            contentCell.bind(dateTimeable);
            taskVboxContent.getChildren().add(root);
        }
    }

    private ContentCell generateContentCell2(DateTimeable dateTimeable) {
        ContentCell cell = null;
        if (dateTimeable instanceof Comment)
            cell = new TaskCommentContentCell();
        else if (dateTimeable instanceof Commit)
            cell = new TaskCommitContentCell();
        else if (dateTimeable instanceof TaskStatus)
            cell = new TaskStatusContentCell();

        if (cell != null) {
            cell.bind(dateTimeable);
        }
        return cell;
    }

    public void updateListMembersView() {
        if (task.getMembers().isEmpty()) {
            taskTextNoMembers.setVisible(true);
            taskListMembers.setVisible(false);
        } else {
            taskTextNoMembers.setVisible(false);
            taskListMembers.setVisible(true);
            taskListMembers.getItems().setAll(task.getMembers());
        }
    }

    public void updateStatusButtons() {
        boolean himself = false;
        boolean next = false;
        for (ToggleButton button : segBtnStatus.getButtons()) {
            if (button.getUserData() == task.getStatus())
                himself = true;

            button.setStyle("");
            button.setOnAction(event -> {});
            button.setDisable(false);

            if (!himself && !next) {
                button.setDisable(true);
            } else if(himself && !next) {
                button.setStyle("-fx-text-fill: white; -fx-background-color: rgb(0, 172, 172);");
                himself = false;
                next = true;
            } else if (!himself && next) {
                himself = false;
                next = false;
                button.setOnAction(event -> applyGitChange((Task.Status) button.getUserData()));
            }
        }
    }
}
