package com.esgi.all4devjava.controller;

import com.esgi.all4devjava.controller.dialog.ErrorDialog;
import com.esgi.all4devjava.menu.MenuItem;
import com.esgi.all4devjava.menu.MenuManager;
import com.esgi.all4devjava.object.Notification;
import com.esgi.all4devjava.object.Project;
import com.esgi.all4devjava.object.User;
import com.esgi.all4devjava.spamapi.SpamRequestApi;
import com.esgi.all4devjava.util.*;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Region;
import javafx.util.Duration;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;
import javafx.scene.shape.*;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RootController extends Controller {

    public static URL layoutURL = Util.getResourceLayout("RootLayout");

    @FXML
    private TreeView<MenuItem> menu;
    @FXML
    private ScrollPane scrollContent;

    public Project project;
    public User user;
    public String authKey;

    MenuManager menuManager;
    public TreeItem<MenuItem> tasksTreeItems;
    ErrorDialog errorDialog;

    GitManager gitManager;
    Timeline timelinePull;

    @Override
    public void initialize() {
        gitManager = new GitManager();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                if (gitManager != null) {
                    gitManager.finish();
                }

                timelinePull.stop();
            }
        });
        timelinePull = new Timeline(new KeyFrame(Duration.seconds(20), event -> {
            //gitManager.put(new GitManagerCommands(new GitManagerCommand<PullResult>("Pull", gitManager.getGit().pull())));
        }));
        timelinePull.setCycleCount(Timeline.INDEFINITE);

        //Clone
        /*try {
            Git.cloneRepository().setURI("ssh://git@127.0.0.1:2222/crabeman/titi")
                    .setTransportConfigCallback(new TransportConfigCallback() {
                        @Override
                        public void configure(Transport transport) {
                            SshTransport sshTransport = (SshTransport) transport;
                            sshTransport.setSshSessionFactory(sshSessionFactory);
                        }
                    })
                    .setDirectory(new File("test")).call();
        } catch (GitAPIException e) {
            e.printStackTrace();
        }*/
        try {errorDialog = Util.loadDialog(getPrimaryStage(), ErrorDialog.class);}
        catch (Exception e) {e.printStackTrace();}
    }

    public void initValue(String authKey, User user, Project project, Settings settings, File projectDir) {
        this.authKey = authKey;
        this.user = user;
        this.project = project;

        gitManager.setProject(project);
        gitManager.setFolder(projectDir);

        gitManager.setPrivateKey(settings.getPrivateKeyPath());
        gitManager.start();

        setSubTitle(project.getName());
        getPrimaryStage().setMinWidth(1200 + 25);
        getPrimaryStage().setMinHeight(600 + getPrimaryStage().getY());

        menuManager = new MenuManager(menu, ((item) -> {
            Region root = item.getController().getRoot();
            //root.setPrefSize(1050, 600);
            root.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);

            scrollContent.setContent(root);
            setSubTitle(item.getName());
        }));

        menuManager.addItem(new MenuItem<ProjectController>("Projet", ProjectController.class) {
            @Override
            public void notify(ProjectController controller) {
                controller.notify(authKey, project, RootController.this);
            }
        });
        tasksTreeItems = menuManager.addItem(new MenuItem<TasksController>("Taches", TasksController.class) {
            @Override
            public void notify(TasksController controller) {
                controller.notify(authKey, project, RootController.this);
            }
        });
        menuManager.addItem(new MenuItem<ProfileController>("Profil", ProfileController.class) {
            @Override
            public void notify(ProfileController controller) {
                controller.notify(authKey, user);
            }
        });
        menuManager.addItem(new MenuItem<NotificationsController>("Notifications", NotificationsController.class) {
            @Override
            public void notify(NotificationsController controller) {
                controller.notify(authKey);
            }


        });




        //Clone
        gitManager.loadOrClone();

        timelinePull.play();
    }
}
