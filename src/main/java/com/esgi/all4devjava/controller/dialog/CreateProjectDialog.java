package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.LoginController;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.object.Project;
import com.esgi.all4devjava.object.Version;
import com.esgi.all4devjava.util.Util;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import retrofit2.Call;
import retrofit2.Response;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CreateProjectDialog extends DialogController {
    public static URL layoutURL = Util.getResourceLayout("CreateProjectDialog");

    @FXML
    private TextField createProjectDialogTextTitle;

    @FXML
    private TextArea createProjectDialogTextDesc;


    private LoginController loginController;
    private Project newProject;
    private Version newProjectVersion;

    @Override
    public void initializeView() {
        setTitle("Creer un projet");
        setOkButton("Ajouter");
        setCancelButton();

        setOkButtonDisable(true);
        createProjectDialogTextTitle.textProperty().addListener((observable, oldValue, newValue) -> {
            setOkButtonDisable(newValue.isEmpty());
        });
    }

    public void clearAndShow(LoginController loginController) {
        this.loginController = loginController;
        createProjectDialogTextTitle.clear();
        createProjectDialogTextDesc.clear();
        setOkButtonDisable(true);
        show();
    }

    @Override
    public void onOk() {
        createProject(createProjectDialogTextTitle.getText(), createProjectDialogTextDesc.getText());
        addVersionNewProject(createProjectDialogTextTitle.getText());
    }

    @Override
    public void onCancel() {

    }

    public void createProject(String title, String desc) {
        if (loginController.apiManager == null || !loginController.apiManager.isAlive()) {
            loginController.apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    newProject = null;
                    setLoading(true);
                }

                @Override
                public void onSuccess() {
                    getDialog().hide();
                    loginController.projects.add(0, newProject);
                    loginController.updateListViewProject();

                }

                @Override
                public void onError(A4DException e) {
                    e.printStackTrace();
                }

                @Override
                public void always() {
                    newProject = null;
                    setLoading(false);
                }
            };
            loginController.apiManager.execute(new ApiRequest<Project>() {
                @Override
                public Call<Project> call(ApiService service) {
                    return service.createProject(loginController.authKey,
                            loginController.user.getUsername(),
                            title,
                            desc);
                }

                @Override
                public void onResponse(Call<Project> call, Response<Project> response) {
                    newProject = response.body();
                }
            });
        }
    }
    public void addVersionNewProject(String title){
        /*loginController.apiManager.execute(new ApiRequest<Version>() {
            @Override
            public Call<Version> call(ApiService service) {
                return service.addVersion(loginController.authKey,
                        loginController.user.getUsername(),
                        title,
                        "0.1");
            }

            @Override
            public void onResponse(Call<Version> call, Response<Version> response) {
                newProjectVersion = response.body();
            }
        });*/
    }


    public <T extends Enum> ToggleButton[] getToggleFromEnum(Class<T> anEnum) {
        List<ToggleButton> toggleButtons = new ArrayList<>();
        for(T t : anEnum.getEnumConstants()) {
            ToggleButton toggleButton = new ToggleButton(t.toString());
            toggleButton.setUserData(t);
            toggleButtons.add(toggleButton);
        }
        return toggleButtons.toArray(new ToggleButton[toggleButtons.size()]);
    }
}
