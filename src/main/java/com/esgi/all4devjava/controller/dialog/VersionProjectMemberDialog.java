package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.controller.ProjectController;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.Project;
import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.object.Version;
import com.esgi.all4devjava.util.Util;
import com.esgi.all4devjava.util.VersionComparator;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javafx.util.Callback;
import retrofit2.Call;

import retrofit2.Call;
import retrofit2.Response;
import java.io.IOException;
import java.net.URL;

public class VersionProjectMemberDialog extends DialogController {
    public static URL layoutURL = Util.getResourceLayout("ProjectVersionDialog");

    @FXML
    private TextField projectCreateVersionDialogTextVersion;


    @FXML
    private Button projectCreateVersionDialogBtnAdd;


    @FXML
    private ListView<String> projectVersionDialogListViewVersion;

    @FXML
    private Button projectCreateVersionDialogBtnDelete;

    private ProjectController projectController;


    private ApiManager apiManager;
    private Member newMember;
    Project project;

    @Override
    public void initializeView() {
        setTitle("Versions");

        //System.out.println(projectController.rootController.authKey);



        projectVersionDialogListViewVersion.setCellFactory(
                new Callback<ListView<String>, ListCell<String>>() {
                    @Override public ListCell<String> call(ListView<String> param) {
                        final ListCell<String> cell = new ListCell<String>() {
                            {
                                super.setPrefWidth(100);
                            }
                            @Override public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item);
                                    setTextFill(Util.versionIsBelowOrEqual(item, projectController.rootController.project.getCurrentVersion()) ? Color.RED : Color.BLACK);
                                }
                                else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }

                });



        projectCreateVersionDialogTextVersion.textProperty().addListener((observable, oldValue, newValue) ->
                projectCreateVersionDialogBtnAdd.setDisable(newValue == null || newValue.length() == 0 || !newValue.matches("^(?:[\\dx]{1,3}\\.){0,3}[\\dx]{1,3}$"))
        );
        projectCreateVersionDialogBtnAdd.setDisable(true);
        projectCreateVersionDialogBtnAdd.setOnAction(event -> onAdd());

        projectVersionDialogListViewVersion.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
                projectCreateVersionDialogBtnDelete.setDisable(newValue == null)
        );
        projectCreateVersionDialogBtnDelete.setDisable(true);
        projectCreateVersionDialogBtnDelete.setOnAction(event -> onDelete());

        setCancelButton();

    }

    public void sortAndShow() {
        //Trie la liste de versions
        Collections.sort(projectController.rootController.project.getVersions(), new VersionComparator());

        projectVersionDialogListViewVersion.getItems().setAll(projectController.rootController.project.getVersions());
    }

    public void onAdd() {
        String versionSaisi = projectCreateVersionDialogTextVersion.getText().toString();
        if (Util.versionIsBelowOrEqual(versionSaisi, projectController.rootController.project.getCurrentVersion())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Erreur lors de l'ajout");
            alert.setContentText("Impossible d'ajouter une version inférieur à celle actuel");

            alert.showAndWait();
        } else {
            addVersion(versionSaisi);
        }
    }

    private void addVersion(String versionSai) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    setLoading(true);
                }

                @Override
                public void onSuccess() {
                    projectController.rootController.project.getVersions().add(versionSai);
                    projectCreateVersionDialogTextVersion.setText("");
                    sortAndShow();
                }

                @Override
                public void onError(A4DException e) {
                }

                @Override
                public void always() {
                    setLoading(false);
                }
            };
            apiManager.execute(new ApiRequest<Version>() {
                @Override
                public Call<Version> call(ApiService service) {
                    System.out.println(projectController.rootController.authKey);
                    return service.addVersion(
                            projectController.rootController.authKey,
                            projectController.rootController.project.getLeaderUsername(),
                            projectController.rootController.project.getName(),
                            versionSai);
                }

                @Override
                public void onResponse(Call<Version> call, Response<Version> response) {
                    //project.setVersions(response.body());
                }
            });
        }

    }

    private void onDelete() {
        int index = projectVersionDialogListViewVersion.getSelectionModel().getSelectedIndex();
        String version = projectController.rootController.project.getVersions().get(index);

        if (Util.versionIsBelowOrEqual(version, projectController.rootController.project.getCurrentVersion())) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Erreur lors de la suppression");
            alert.setContentText("Impossible de supprimer une version inférieur à celle actuel");

            alert.showAndWait();
        } else {

            List<Task> blockedTasks = new ArrayList<>();
            for (Task task : project.getTasks())
                if (task.getVersion().equalsIgnoreCase(version)) {
                    blockedTasks.add(task);
                }

            if (blockedTasks.size() == 0) {
                onRemoveVersion(index, version);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Erreur");
                alert.setHeaderText("Erreur lors de la suppression");
                StringBuffer s = new StringBuffer("Cette version est définie dans les taches :");
                for (Task task : blockedTasks) {
                    s.append("\n" + task.getName() + " -> " + task.getStatus());
                }
                alert.getDialogPane().setContent(new Label(s.toString()));

                alert.showAndWait();
            }
        }
    }

    private void onRemoveVersion(int index, String version) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    setLoading(true);
                }

                @Override
                public void onSuccess() {
                    projectController.rootController.project.getVersions().remove(index);
                    projectVersionDialogListViewVersion.getSelectionModel().clearSelection();
                    sortAndShow();
                }

                @Override
                public void onError(A4DException e) {

                }

                @Override
                public void always() {
                    setLoading(false);
                }
            };
            apiManager.execute(new ApiRequest<Void>(){
                @Override
                public Call<Void> call(ApiService service) {
                    return service.removeVersion(
                            projectController.rootController.authKey,
                            projectController.rootController.project.getLeaderUsername(),
                            projectController.rootController.project.getName(),
                            version);
                }

                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                }
            });
        }
    }


    public void notifyAndShow(ProjectController projectController) {
        this.projectController = projectController;
        getDialog().show();
        sortAndShow();
    }

}
