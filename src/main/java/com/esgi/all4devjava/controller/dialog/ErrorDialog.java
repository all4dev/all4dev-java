package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.net.URL;

public class ErrorDialog extends DialogController {
    public static URL layoutURL = Util.getResourceLayout("ErrorDialog");


    @FXML
    Label errorDialogTextErr;

    @Override
    public void initializeView() {
        setTitle("Oops");
        setOkButton("Ok");
    }

    @Override
    public void onOk() {
        getDialog().hide();
    }

    @Override
    public void onCancel() {

    }

    public void notifyAndShow(String error) {
        errorDialogTextErr.setText(error);
        getDialog().show();
    }
}
