package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.ProjectController;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.list.cell.UsersListCell;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.User;
import com.esgi.all4devjava.util.Util;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import org.apache.commons.validator.routines.EmailValidator;
import org.controlsfx.control.SegmentedButton;
import retrofit2.Call;
import retrofit2.Response;

import java.net.URL;
import java.util.List;

public class RemoveProjectMemberDialog extends DialogController {
    public static URL layoutURL = Util.getResourceLayout("RemoveProjectMemberDialog");

    @FXML
    private Label removeProjectMemberDialogLabel;


    private ProjectController projectController;

    private ApiManager apiManager;
    private Member member;
    private List<User> searchedUser;

    @Override
    public void initializeView() {
        setTitle("Ajouter un membre");
        setOkButton("Supprimer");
        setCancelButton();
    }

    @Override
    public void onOk() {
        removeMember(member);
    }

    @Override
    public void onCancel() {

    }

    private void removeMember(Member member) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    setLoading(true);
                }

                @Override
                public void onSuccess() {
                    getDialog().hide();
                    projectController.rootController.project.getMembers().remove(RemoveProjectMemberDialog.this.member);
                    projectController.updateMemberListView();
                }

                @Override
                public void onError(A4DException e) {
                }

                @Override
                public void always() {
                    RemoveProjectMemberDialog.this.member = null;
                    setLoading(false);
                }
            };
            apiManager.execute(new ApiRequest<Void>() {
                @Override
                public Call<Void> call(ApiService service) {
                    return service.removeProjectMember(
                            projectController.rootController.authKey,
                            projectController.rootController.project.getLeaderUsername(),
                            projectController.rootController.project.getName(),
                            member.getUsername());
                }

                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {
                }
            });
        }

    }

    public void notifyAndShow(ProjectController projectController, Member member) {
        this.projectController = projectController;
        this.member = member;
        removeProjectMemberDialogLabel.setText("Êtes vous sur de vouloir supprimer " + member.getUsername() + " du projet ?");
        getDialog().show();
    }
}
