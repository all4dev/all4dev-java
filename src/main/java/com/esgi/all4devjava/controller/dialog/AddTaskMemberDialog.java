package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.TaskController;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.list.cell.UsersListCell;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.TaskMember;
import com.esgi.all4devjava.object.User;
import com.esgi.all4devjava.util.Util;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import retrofit2.Call;
import retrofit2.Response;

import java.net.URL;
import java.util.List;

public class AddTaskMemberDialog extends DialogController {
    public static URL layoutURL = Util.getResourceLayout("AddTaskMemberDialog");

    @FXML
    private ListView<User> addTaskMemberDialogList;

    private TaskController taskController;
    private TaskMember newMember;

    @Override
    public void initializeView() {
        setTitle("Ajouter un membre");
        setOkButton("Ajouter");
        setCancelButton();

        addTaskMemberDialogList.setItems(FXCollections.observableArrayList());
        addTaskMemberDialogList.setCellFactory(param -> new UsersListCell());

        setOkButtonDisable(true);
        addTaskMemberDialogList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            setOkButtonDisable(newValue == null);
        });
    }

    public void notifyAndShow(TaskController taskController, List<Member> members) {
        this.taskController = taskController;
        addTaskMemberDialogList.getSelectionModel().clearSelection();
        addTaskMemberDialogList.getItems().setAll(members);
        getDialog().show();
    }

    @Override
    public void onOk() {
        addMember(addTaskMemberDialogList.getSelectionModel().getSelectedItem());
    }

    @Override
    public void onCancel() {

    }

    public void addMember(User user) {
        if (taskController.apiManager == null || !taskController.apiManager.isAlive()) {
            taskController.apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    newMember = null;
                    setLoading(true);
                }

                @Override
                public void onSuccess() {
                    getDialog().hide();
                    taskController.task.getMembers().add(newMember);
                    taskController.updateListMembersView();
                }

                @Override
                public void onError(A4DException e) {
                }

                @Override
                public void always() {
                    newMember = null;
                    setLoading(false);
                }
            };
            taskController.apiManager.execute(new ApiRequest<TaskMember>() {
                @Override
                public Call<TaskMember> call(ApiService service) {
                    return service.addTaskMember(taskController.authKey, taskController.project.getLeaderUsername(), taskController.project.getName(), taskController.task.getId(), user.getUsername());
                }

                @Override
                public void onResponse(Call<TaskMember> call, Response<TaskMember> response) {
                    newMember = response.body();
                }
            });
        }

    }
}
