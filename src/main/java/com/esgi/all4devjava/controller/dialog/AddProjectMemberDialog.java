package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.ProjectController;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.list.cell.UsersListCell;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.object.User;
import com.esgi.all4devjava.util.Util;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import org.apache.commons.validator.routines.EmailValidator;
import org.controlsfx.control.SegmentedButton;
import retrofit2.Call;
import retrofit2.Response;

import java.net.URL;
import java.util.List;

public class AddProjectMemberDialog extends DialogController {
    public static URL layoutURL = Util.getResourceLayout("AddProjectMemberDialog");


    @FXML
    private ListView<User> addProjectMemberDialogList;




    private ProjectController projectController;


    private ApiManager apiManager;
    private Member newMember;
    private List<User> searchedUser;

    @Override
    public void initializeView() {
        setTitle("Ajouter un membre");
        setOkButton("Ajouter");
        setCancelButton();

    }

    @Override
    public void initDialog() {
        super.initDialog();
        updateOkButton();
    }

    @Override
    public void onOk() {
    }

    @Override
    public void onCancel() {

    }

    private void updateOkButton() {

    }

    private void onSearch(String search) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    setLoading(true);
                }

                @Override
                public void onSuccess() {
                    if (searchedUser.size() == 0) {
                        System.out.println("ok");
                    } else {
                        System.out.println("pasok");
                    }
                }

                @Override
                public void onError(A4DException e) {

                }

                @Override
                public void always() {
                    setLoading(false);
                }
            };

            apiManager.execute(new ApiRequest<List<User>>() {
                @Override
                public Call<List<User>> call(ApiService service) {
                    boolean isEmail = EmailValidator.getInstance().isValid(search);
                    return service.getUsers(projectController.rootController.authKey,
                            isEmail ? null : search,
                            isEmail ? search: null);
                }

                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    searchedUser = response.body();
                    searchedUser.removeAll(projectController.rootController.project.getMembers());
                }
            });
        }
    }

    private void addMember(User user, Member.Access access) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    newMember = null;
                    setLoading(true);
                }

                @Override
                public void onSuccess() {
                    getDialog().hide();
                    projectController.rootController.project.getMembers().add(newMember);
                    projectController.updateMemberListView();
                }

                @Override
                public void onError(A4DException e) {
                }

                @Override
                public void always() {
                    newMember = null;
                    setLoading(false);
                }
            };
            apiManager.execute(new ApiRequest<Member>() {
                @Override
                public Call<Member> call(ApiService service) {
                    return service.addProjectMember(
                            projectController.rootController.authKey,
                            projectController.rootController.project.getLeaderUsername(),
                            projectController.rootController.project.getName(),
                            user.getUsername(),
                            access.name());
                }

                @Override
                public void onResponse(Call<Member> call, Response<Member> response) {
                    newMember = response.body();
                }
            });
        }

    }

    public void notifyAndShow(ProjectController projectController) {
        this.projectController = projectController;
        getDialog().show();
    }
}
