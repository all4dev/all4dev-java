package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.ProjectController;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.User;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.StackPane;
import org.controlsfx.control.SegmentedButton;
import retrofit2.Call;
import retrofit2.Response;

import java.net.URL;

public class UpdateProjectMemberDialog extends DialogController {
    public static URL layoutURL = Util.getResourceLayout("UpdateProjectMemberDialog");

    @FXML
    private Label updateProjectMemberDialogLabelUsername;

    @FXML
    private StackPane updateProjectMemberDialogStackType;

    private SegmentedButton segmentedBtnType;


    private ProjectController projectController;


    private ApiManager apiManager;
    private Member member;
    private Member newMember;

    @Override
    public void initializeView() {
        setTitle("Ajouter un membre");
        setOkButton("Modifier");
        setCancelButton();

        segmentedBtnType = new SegmentedButton(Util.getToggleFromEnum(Member.Access.class));
        updateProjectMemberDialogStackType.getChildren().add(segmentedBtnType);
        segmentedBtnType.getToggleGroup().selectedToggleProperty().addListener((observable, oldValue, newValue) -> updateOkButton());
    }

    @Override
    public void initDialog() {
        super.initDialog();
        updateOkButton();
    }

    @Override
    public void onOk() {
        updateMember(member, (Member.Access) segmentedBtnType.getToggleGroup().getSelectedToggle().getUserData());
    }

    @Override
    public void onCancel() {

    }

    private void updateOkButton() {
        setOkButtonDisable(segmentedBtnType.getToggleGroup().getSelectedToggle() == null || segmentedBtnType.getToggleGroup().getSelectedToggle().getUserData() == member.getAccess());
    }

    private void updateMember(User user, Member.Access access) {
        if (apiManager == null || !apiManager.isAlive()) {
            apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    setLoading(true);
                    newMember = null;
                }

                @Override
                public void onSuccess() {
                    getDialog().hide();
                    projectController.rootController.project.getMembers().remove(member);
                    projectController.rootController.project.getMembers().add(newMember);
                    projectController.updateMemberListView();
                }

                @Override
                public void onError(A4DException e) {
                }

                @Override
                public void always() {
                    newMember = null;
                    member = null;
                    setLoading(false);
                }
            };
            apiManager.execute(new ApiRequest<Member>() {
                @Override
                public Call<Member> call(ApiService service) {
                    return service.updateProjectMember(
                            projectController.rootController.authKey,
                            projectController.rootController.project.getLeaderUsername(),
                            projectController.rootController.project.getName(),
                            user.getUsername(),
                            access.name());
                }

                @Override
                public void onResponse(Call<Member> call, Response<Member> response) {
                    newMember = response.body();
                }
            });
        }

    }

    public void notifyAndShow(ProjectController projectController, Member member) {
        this.projectController = projectController;
        this.member = member;
        updateProjectMemberDialogLabelUsername.setText(member.getUsername());
        getDialog().show();
        Util.toggleUserData(segmentedBtnType, member.getAccess());
    }
}
