package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.TasksController;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.util.Util;
import com.esgi.all4devjava.util.VersionComparator;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.util.Callback;
import org.controlsfx.control.SegmentedButton;
import retrofit2.Call;
import retrofit2.Response;

import java.net.URL;
import java.util.Collections;

public class CreateTaskDialog extends DialogController {
    public static URL layoutURL = Util.getResourceLayout("CreateTaskDialog");

    @FXML
    private TextField createTaskDialogTextTitle;

    @FXML
    private TextField createTaskDialogTextVersion;

    @FXML
    private StackPane createTaskDialogStackType;

    @FXML
    private ComboBox createTaskDialogComboBoxVersion;

    private SegmentedButton segmentedBtnType;

    @FXML
    private TextArea createTaskDialogTextDesc;


    private TasksController tasksController;
    private Task newTask;

    @Override
    public void initializeView() {
        setTitle("Creer une tache");
        setOkButton("Ajouter");
        setCancelButton();

        createTaskDialogComboBoxVersion.setCellFactory(
                new Callback<ListView<String>, ListCell<String>>() {
                    @Override public ListCell<String> call(ListView<String> param) {
                        final ListCell<String> cell = new ListCell<String>() {
                            {
                                super.setPrefWidth(100);
                            }
                            @Override public void updateItem(String item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item != null) {
                                    setText(item);
                                }
                                else {
                                    setText(null);
                                }
                            }
                        };
                        return cell;
                    }

                });
        createTaskDialogComboBoxVersion.setEditable(true);

        segmentedBtnType = new SegmentedButton(Util.getToggleFromEnum(Task.Type.class));
        createTaskDialogStackType.getChildren().add(segmentedBtnType);
        /*addTaskMemberDialogList.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            dialogConfirmBtn.setDisable(newValue == null);
        });*/
    }

    public void sortAndShow() {
        //Trie la liste de versions
        Collections.sort(tasksController.project.getVersions(), new VersionComparator());
        createTaskDialogComboBoxVersion.getItems().clear();
        for (String version : tasksController.project.getVersions()) {
            if (!Util.versionIsBelowOrEqual(version, tasksController.project.getCurrentVersion()))
                createTaskDialogComboBoxVersion.getItems().add(version);
        }
    }

    public void clearAndShow(TasksController tasksController) {
        this.tasksController = tasksController;
        createTaskDialogTextTitle.clear();
        segmentedBtnType.getToggleGroup().selectToggle(null);
        createTaskDialogTextDesc.clear();
        sortAndShow();
        show();
    }

    @Override
    public void onOk() {
        createTask(createTaskDialogTextTitle.getText(), (Task.Type) segmentedBtnType.getToggleGroup().getSelectedToggle().getUserData(), createTaskDialogComboBoxVersion.getSelectionModel().getSelectedItem().toString(), createTaskDialogTextDesc.getText());
    }

    @Override
    public void onCancel() {

    }

    public void createTask(String title, Task.Type taskType, String taskVersion, String desc) {

        if (tasksController.apiManager == null || !tasksController.apiManager.isAlive()) {
            tasksController.apiManager = new ApiManager() {
                @Override
                public void onPreExec() {
                    newTask = null;
                    setLoading(true);
                }

                @Override
                public void onSuccess() {
                    getDialog().hide();
                    tasksController.project.getTasks().add(newTask);
                    tasksController.updateListTaskView();
                }

                @Override
                public void onError(A4DException e) {
                    e.printStackTrace();
                }

                @Override
                public void always() {
                    newTask = null;
                    setLoading(false);

                }
            };
            tasksController.apiManager.execute(new ApiRequest<Task>() {
                @Override
                public Call<Task> call(ApiService service) {
                    return service.createTask(tasksController.authKey,
                            tasksController.project.getLeaderUsername(),
                            tasksController.project.getName(),
                            title,
                            taskType.name(),
                            taskVersion,
                            desc);
                }

                @Override
                public void onResponse(Call<Task> call, Response<Task> response) {
                    newTask = response.body();
                }
            });
        }
    }
}
