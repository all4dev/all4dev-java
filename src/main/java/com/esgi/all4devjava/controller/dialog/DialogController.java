package com.esgi.all4devjava.controller.dialog;

import com.esgi.all4devjava.controller.Controller;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;

abstract public class DialogController extends Controller {
    private Dialog dialog;


    private ButtonType okButtonType;
    private Button okButton;

    private ButtonType cancelButtonType;
    private Button cancelButton;

    private StackPane loader;

    public void initDialog() {
        StackPane mainRoot = new StackPane();
        mainRoot.setPadding(new Insets(20,10, 20, 10));

        loader = new StackPane();
        loader.setStyle("-fx-background-color:  rgba(0, 0, 0, 0.3);");
        ProgressIndicator progressIndicator = new ProgressIndicator();
        progressIndicator.setMaxSize(80, 80);
        loader.getChildren().add(progressIndicator);

        mainRoot.getChildren().addAll(getRoot(), loader);
        setLoading(false);
        dialog.getDialogPane().setContent(mainRoot);
    }

    public Dialog getDialog() {
        return dialog;
    }

    @Override
    public final void show() {
        dialog.show();
    }

    @Override
    public final void initialize() {
        dialog = new Dialog();
        initializeView();
    }

    public void onOk() {

    }

    public void onCancel() {

    }

    public void setTitle(String title) {
        dialog.setTitle(title);
    }

    public abstract void initializeView();

    public void setOkButton() {
        setOkButton("Confirmer");
    }

    public void setOkButton(String s) {
        okButtonType = new ButtonType(s, ButtonBar.ButtonData.OK_DONE);
        getDialog().getDialogPane().getButtonTypes().add(okButtonType);
        okButton = (Button) getDialog().getDialogPane().lookupButton(okButtonType);
        okButton.addEventFilter(ActionEvent.ACTION, event -> {
            event.consume();
            onOk();
        });
    }

    public void setCancelButton() {
        setCancelButton("Fermer");
    }

    public void setCancelButton(String s) {
        cancelButtonType = new ButtonType(s, ButtonBar.ButtonData.CANCEL_CLOSE);
        getDialog().getDialogPane().getButtonTypes().add(cancelButtonType);
        cancelButton = (Button) getDialog().getDialogPane().lookupButton(cancelButtonType);
        cancelButton.addEventFilter(ActionEvent.ACTION, event -> {
            onCancel();
        });
    }


    public void setOkButtonDisable(boolean disable) {
        okButton.setDisable(disable);
    }

    public void setLoading(boolean loading) {
        if (okButton != null)
            okButton.setDisable(loading);
        loader.setVisible(loading);
    }

    public Button getOkButton() {
        return okButton;
    }
}
