package com.esgi.all4devjava.controller;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.controller.dialog.CreateTaskDialog;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.list.cell.TasksListCell;
import com.esgi.all4devjava.menu.MenuItem;
import com.esgi.all4devjava.menu.TaskMenuItem;
import com.esgi.all4devjava.object.Project;
import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.util.DateTimeComparator;
import com.esgi.all4devjava.util.Util;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TreeItem;
import retrofit2.Call;
import retrofit2.Response;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TasksController extends FragmentController {
    public static URL layoutURL = Util.getResourceLayout("TasksView");

    @FXML
    Button tasksBtnAdd;

    @FXML
    Button tasksBtnReload;

    @FXML
    ListView<Task> tasksListView;


    public String authKey;
    public Project project;
    RootController rootController;

    public ApiManager apiManager;
    CreateTaskDialog createTaskDialog;

    @Override
    public void initialize() {
        tasksListView.setCellFactory(
                listView -> new TasksListCell() {
                    @Override
                    public void onClick(Task task) {
                        for (TreeItem<MenuItem> treeItem : rootController.tasksTreeItems.getChildren()) {
                            MenuItem menuItem = treeItem.getValue();
                            if (menuItem instanceof TaskMenuItem) {
                                if (((TaskMenuItem) menuItem).getTask().equals(task)) {
                                    rootController.menuManager.forceClick(treeItem);
                                }
                            }
                        }
                    }
                }
        );
        tasksListView.setItems(FXCollections.observableArrayList());
        try {
            createTaskDialog = Util.loadDialog(getPrimaryStage(), CreateTaskDialog.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        tasksBtnAdd.setOnAction(event -> createTaskDialog.clearAndShow(TasksController.this));
        tasksBtnReload.setOnAction(event -> pullTasks());
    }

    private void pullTasks() {
        apiManager = new ApiManager() {
            @Override
            public void onPreExec() {
                tasksBtnReload.setDisable(true);
            }

            @Override
            public void onSuccess() {
                updateListTaskView();
            }

            @Override
            public void onError(A4DException e) {

            }

            @Override
            public void always() {
                tasksBtnReload.setDisable(false);
            }
        };
        apiManager.execute(new ApiRequest<List<Task>>() {

            @Override
            public Call<List<Task>> call(ApiService service) {
                return service.getTasks(authKey, project.getLeaderUsername(), project.getName());
            }

            @Override
            public void onResponse(Call<List<Task>> call, Response<List<Task>> response) {
                project.setTasks(response.body());
            }
        });
    }

    public void notify(String authKey, Project project, RootController rootController) {
        this.authKey = authKey;
        this.project = project;
        this.rootController = rootController;
        updateListTaskView();
    }

    public void updateListTaskView() {
        tasksListView.getItems().clear();
        rootController.tasksTreeItems.getChildren().clear();
        if (project.getTasks().size() > 0) {
            Collections.sort(project.getTasks(), new DateTimeComparator<Task>(DateTimeComparator.Sort.DESC));
            tasksListView.getItems().setAll(project.getTasks());
            for (Task task : project.getTasks()) {
                rootController.tasksTreeItems.getChildren().add(new TreeItem<MenuItem>(new TaskMenuItem(rootController, authKey, project, task)));
            }
        } else {
            //Todo: No Task
        }
    }
}
