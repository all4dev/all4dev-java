package com.esgi.all4devjava.list.cell;

import com.esgi.all4devjava.list.data.ContentCell;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.input.MouseEvent;

interface DataCellable<T> {
    ContentCell<T> getContentCell(T t);
    MultipleSelectionModel<T> getSelectionModel();
    void onClick(T t);
    void onRightClick(T t, MouseEvent mouseEvent);
}
