package com.esgi.all4devjava.list.cell;

import com.esgi.all4devjava.list.data.ContentCell;
import com.esgi.all4devjava.list.data.MembersProjectTileContentCell;
import com.esgi.all4devjava.object.Member;

public class MembersProjectTileListCell extends DataListCell<Member> {

    @Override
    public ContentCell<Member> getContentCell(Member t) {
        return new MembersProjectTileContentCell();
    }
}
