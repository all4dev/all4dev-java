package com.esgi.all4devjava.list.cell;


import com.esgi.all4devjava.list.data.ContentCell;
import com.esgi.all4devjava.list.data.TaskCommentContentCell;
import com.esgi.all4devjava.list.data.TaskCommitContentCell;
import com.esgi.all4devjava.list.data.TaskStatusContentCell;
import com.esgi.all4devjava.object.Comment;
import com.esgi.all4devjava.object.Commit;
import com.esgi.all4devjava.object.TaskStatus;

public class TaskListCell extends DataListCell<Object> {
    @Override
    public ContentCell<Object> getContentCell(Object object) {
        if (object instanceof Commit)
            return (ContentCell) new TaskCommitContentCell();
        else if (object instanceof Comment)
            return (ContentCell) new TaskCommentContentCell();
        else if (object instanceof TaskStatus)
            return (ContentCell) new TaskStatusContentCell();
        return null;
    }
}
