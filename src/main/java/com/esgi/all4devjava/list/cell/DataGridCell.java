package com.esgi.all4devjava.list.cell;

import javafx.scene.Node;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.input.MouseEvent;
import org.controlsfx.control.GridCell;

public abstract class DataGridCell<T> extends GridCell<T> implements DataCellable<T>{

    private DataCell<T> dataCell;

    public DataGridCell () {
        super();
        dataCell = new DataCell<T>(this);
    }

    @Override
    public void updateItem(T t, boolean empty) {
        Node root = dataCell.updateItem(t);
        setGraphic(empty ? null : root);
    }

    @Override
    public MultipleSelectionModel<T> getSelectionModel() {
        return null;
    }

    @Override
    public void onClick(T t) {
        //Override if needed
    }

    @Override
    public void onRightClick(T t, MouseEvent mouseEvent) {
        //Override if needed
    }
}
