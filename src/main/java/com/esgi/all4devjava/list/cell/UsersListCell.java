package com.esgi.all4devjava.list.cell;

import com.esgi.all4devjava.list.data.ContentCell;
import com.esgi.all4devjava.list.data.UsersContentCell;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.User;

public class UsersListCell extends DataListCell<User> {

    @Override
    public ContentCell<User> getContentCell(User t) {
        return new UsersContentCell();
    }
}
