package com.esgi.all4devjava.list.cell;

import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.MultipleSelectionModel;
import javafx.scene.input.MouseEvent;

public abstract class DataListCell<T> extends ListCell<T> implements DataCellable<T> {
    private DataCell<T> dataCell;

    public DataListCell() {
        super();
        dataCell = new DataCell<T>(this);
    }

    @Override
    public void updateItem(T t, boolean empty) {
        super.updateItem(t, empty);
        if (empty || t == null) {
            if (getGraphic() != null)
                getGraphic().setVisible(false);
            setGraphic(null);
        } else {
            Node root = dataCell.updateItem(t);
            root.setVisible(true);
            setGraphic(root);
        }
    }

    @Override
    public MultipleSelectionModel<T> getSelectionModel() {
        return getListView().getSelectionModel();
    }

    @Override
    public void onClick(T t) {
        //Override if needed
    }

    @Override
    public void onRightClick(T t, MouseEvent mouseEvent) {
        //Override if needed
    }
}
