package com.esgi.all4devjava.list.cell;

import com.esgi.all4devjava.list.data.ContentCell;
import com.esgi.all4devjava.list.data.ProjectContentCell;
import com.esgi.all4devjava.object.Project;

public abstract class ProjectListCell extends DataListCell<Project> {

    @Override
    public final ContentCell<Project> getContentCell(Project t) {
        return new ProjectContentCell();
    }
}
