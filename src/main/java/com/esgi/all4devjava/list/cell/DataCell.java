package com.esgi.all4devjava.list.cell;

import com.esgi.all4devjava.list.data.ContentCell;
import javafx.scene.Node;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

public class DataCell<T> {

    DataCellable<T> dataCellable;
    ContentCell<T> dataCell;

    public DataCell(DataCellable<T> dataCellable) {
        this.dataCellable = dataCellable;
    }

    public Node updateItem(T t) {
        if (t != null) {
            if (dataCell == null) {
                dataCell = dataCellable.getContentCell(t);
            }
            dataCell.getRoot().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
                if (event.getButton() == MouseButton.PRIMARY) {
                    if (dataCellable.getSelectionModel() != null)
                        dataCellable.getSelectionModel().select(t);
                    dataCellable.onClick(t);
                } else if (event.getButton() == MouseButton.SECONDARY) {
                    dataCellable.onRightClick(t, event);
                }
            });
            dataCell.bind(t);
            return dataCell.getRoot();
        } else return null;
    }
}
