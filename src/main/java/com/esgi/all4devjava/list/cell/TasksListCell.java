package com.esgi.all4devjava.list.cell;

import com.esgi.all4devjava.list.data.ContentCell;
import com.esgi.all4devjava.list.data.TasksContentCell;
import com.esgi.all4devjava.object.Task;

public abstract class TasksListCell extends DataListCell<Task> {

    @Override
    public ContentCell<Task> getContentCell(Task task) {
        return new TasksContentCell();
    }
}
