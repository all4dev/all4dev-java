package com.esgi.all4devjava.list.cell;

import com.esgi.all4devjava.list.data.ContentCell;
import com.esgi.all4devjava.list.data.MembersProjectAccessTileContentCell;
import com.esgi.all4devjava.object.Member;

public abstract class MembersProjectAccessTileGridCell extends DataGridCell<Member> {

    @Override
    public ContentCell<Member> getContentCell(Member t) {
        return new MembersProjectAccessTileContentCell();
    }
}
