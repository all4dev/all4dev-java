package com.esgi.all4devjava.list.data;

import com.esgi.all4devjava.object.Commit;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.Text;


public class TaskCommitContentCell extends ContentCell<Commit> {

    @FXML
    private Label taskCommitCell_author;

    @FXML
    private Label taskCommitCell_date;

    @FXML
    private Text taskCommitCell_body;

    @FXML
    private Label taskCommitCell_hash;

    public TaskCommitContentCell() {
        super(Util.getResourceItem("TaskCommitContentCell"));
    }

    @Override
    public void bind(Commit commit) {
        taskCommitCell_author.setText(commit.getAuthorName());
        Util.setTooltip(taskCommitCell_author, commit.getAuthorName() + "@" + commit.getAuthorEmail());

        taskCommitCell_date.setText(Util.getDateFormated(commit.getDate()));
        Util.setTooltip(taskCommitCell_date, commit.getDate().toString());

        taskCommitCell_body.setText(commit.getSubject());
        taskCommitCell_hash.setText(commit.getHash().substring(0, 7));
    }
}
