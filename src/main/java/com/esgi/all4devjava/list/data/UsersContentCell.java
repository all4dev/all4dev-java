package com.esgi.all4devjava.list.data;

import com.esgi.all4devjava.imageloader.ImageLoader;
import com.esgi.all4devjava.object.User;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

public class UsersContentCell extends ContentCell<User> {

    @FXML
    private ImageView userCell_image;

    @FXML
    private Label userCell_username;

    @FXML
    private Label userCell_name;

    public UsersContentCell() {
        super(Util.getResourceItem("UsersContentCell"));
    }

    @Override
    public void bind(User user) {
        ImageLoader.put(userCell_image, user.getUrlPhotoPath());
        userCell_username.setText(user.getUsername());
        userCell_name.setText(user.getFirstname() + " " + user.getLastname());
    }
}
