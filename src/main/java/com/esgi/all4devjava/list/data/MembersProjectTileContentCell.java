package com.esgi.all4devjava.list.data;

import com.esgi.all4devjava.imageloader.ImageLoader;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import java.net.URL;

public class MembersProjectTileContentCell extends ContentCell<Member> {

    @FXML
    private ImageView membersProjectTileCell_image;

    @FXML
    private Label membersProjectTileCell_username;

    public MembersProjectTileContentCell() {
        super(Util.getResourceItem("MembersProjectTileContentCell"));
    }

    public MembersProjectTileContentCell(URL url) {
        super(url);
    }

    @Override
    public void bind(Member member) {
        ImageLoader.put(membersProjectTileCell_image, member.getUrlPhotoPath());
        membersProjectTileCell_username.setText(member.getUsername());
    }
}
