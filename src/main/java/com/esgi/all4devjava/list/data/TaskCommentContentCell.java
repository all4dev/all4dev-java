package com.esgi.all4devjava.list.data;

import com.esgi.all4devjava.object.Comment;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

public class TaskCommentContentCell extends ContentCell<Comment> {

    @FXML
    private Label taskCommentCell_author;

    @FXML
    private Label taskCommentCell_date;

    @FXML
    private Text taskCommentCell_body;

    public TaskCommentContentCell() {
        super(Util.getResourceItem("TaskCommentContentCell"));
    }

    @Override
    public void bind(Comment comment) {
        taskCommentCell_author.setText(comment.getAuthorName());

        taskCommentCell_date.setText(Util.getDateFormated(comment.getCreationDate()));
        Util.setTooltip(taskCommentCell_date, comment.getCreationDate().toString());

        taskCommentCell_body.setText(comment.getBody());
    }
}
