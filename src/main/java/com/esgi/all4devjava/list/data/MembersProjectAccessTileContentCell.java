package com.esgi.all4devjava.list.data;

import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class MembersProjectAccessTileContentCell extends MembersProjectTileContentCell {

    @FXML
    private Label membersAccessProjectTileCell_type;

    public MembersProjectAccessTileContentCell() {
        super(Util.getResourceItem("MembersProjectAccessTileContentCell"));
    }

    @Override
    public void bind(Member member) {
        super.bind(member);
        membersAccessProjectTileCell_type.setText(member.getAccess().toString());
    }
}
