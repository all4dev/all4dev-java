package com.esgi.all4devjava.list.data;

import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.Text;

public class TasksContentCell extends ContentCell<Task> {

    @FXML
    private Label tasksCell_title;

    @FXML
    private Label tasksCell_author;

    @FXML
    private Label tasksCell_members;

    @FXML
    private Label tasksCell_lastUpdate;

    @FXML
    private Label tasksCell_desc;

    public TasksContentCell() {
        super(Util.getResourceItem("TasksContentCell"));
    }

    @Override
    public final void bind(Task task) {
        tasksCell_title.setText(task.getName());
        tasksCell_author.setText(task.getAuthorName());
        //tasksCell_members.setText(task.getMembers().size());
        tasksCell_lastUpdate.setText(Util.getDateFormated(task.getLastUpdate()));
        tasksCell_desc.setText(task.getBody());
    }
}
