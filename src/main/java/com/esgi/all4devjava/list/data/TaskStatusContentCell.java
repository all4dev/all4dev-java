package com.esgi.all4devjava.list.data;

import com.esgi.all4devjava.object.TaskStatus;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;


public class TaskStatusContentCell extends ContentCell<TaskStatus> {

    @FXML
    private Label taskStatusCell_status;

    @FXML
    private Label taskStatusCell_date;

    public TaskStatusContentCell() {
        super(Util.getResourceItem("TaskStatusContentCell"));
    }

    @Override
    public void bind(TaskStatus status) {
        taskStatusCell_status.setText(status.getStatus().getDisplay());
        taskStatusCell_date.setText(Util.getDateFormated(status.getCreationDate()));
    }
}
