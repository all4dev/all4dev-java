package com.esgi.all4devjava.list.data;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

import java.io.IOException;
import java.net.URL;

public abstract class ContentCell<T> {
    protected Node root;

    public ContentCell(URL url) {
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setController(this);
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public abstract void bind(T t);

    public Node getRoot() {
        return root;
    }
}
