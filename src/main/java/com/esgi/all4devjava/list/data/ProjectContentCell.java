package com.esgi.all4devjava.list.data;

import com.esgi.all4devjava.object.Project;
import com.esgi.all4devjava.util.Util;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class ProjectContentCell extends ContentCell<Project> {

    @FXML
    private Label projectCell_name;

    @FXML
    private Label projectCell_owner;

    @FXML
    private Label projectCell_lastUpdate;

    public ProjectContentCell() {
        super(Util.getResourceItem("ProjectContentCell"));
    }

    @Override
    public void bind(Project project) {
        projectCell_name.setText(project.getName());
        projectCell_owner.setText(project.getLeaderUsername());
        projectCell_lastUpdate.setText(Util.getDateFormated(project.getLastUpdate()));
    }
}
