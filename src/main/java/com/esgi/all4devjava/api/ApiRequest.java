package com.esgi.all4devjava.api;

import retrofit2.Call;
import retrofit2.Response;

public abstract class ApiRequest<T> {
    public abstract Call<T> call(ApiService service);

    public abstract void onResponse(Call<T> call, Response<T> response);

    public Call<T> getCall(ApiService apiService) {
        return call(apiService);
    }
}
