package com.esgi.all4devjava.api;

import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.exception.NetworkException;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.util.gsonadapter.DateTimeAdapter;
import com.esgi.all4devjava.util.gsonadapter.EnumAdapter;
import com.google.gson.GsonBuilder;
import javafx.application.Platform;
import okhttp3.OkHttpClient;
import org.joda.time.DateTime;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public abstract class ApiManager extends Thread {

    private ApiRequest[] requests;

    public abstract void onPreExec();

    public abstract void onSuccess();

    public abstract void onError(A4DException e);

    public abstract void always();

    public void execute(ApiRequest... requests) {
        onPreExec();
        this.requests = requests;
        start();
    }

    @Override
    public void run() {
        A4DException error = null;
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build();

        ApiService apiService = new Retrofit.Builder()
                .baseUrl(ApiService.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(
                        new GsonBuilder()
                                .registerTypeAdapter(DateTime.class, new DateTimeAdapter())
                                .registerTypeAdapter(Member.Access.class, new EnumAdapter<Member.Access>(Member.Access.class))
                                .registerTypeAdapter(Task.Status.class, new EnumAdapter<Task.Status>(Task.Status.class))
                                .registerTypeAdapter(Task.Type.class, new EnumAdapter<Task.Type>(Task.Type.class))
                                .create())
                )
                .build()
                .create(ApiService.class);

        for (ApiRequest request : requests) {
            try {
                Call call = request.getCall(apiService);
                if (call == null)
                    continue;
                Response response = call.execute();
                if (response.isSuccessful()) {
                    request.onResponse(call, response);
                } else { //TODO: Throw the good Exception
                    System.out.println(response.errorBody().string());
                    System.out.println(response.code());
                    System.out.println(call.request().url());
                    error = new A4DException(response.code());
                    break;
                }
            } catch (IOException e) {
                error = new NetworkException();
                break;
            }
        }
        A4DException finalError = error;
        Platform.runLater(() -> {
            if (finalError == null)
                onSuccess();
            else
                onError(finalError);
            always();
        });

    }
}
