package com.esgi.all4devjava.api;

import com.esgi.all4devjava.object.*;
import com.esgi.all4devjava.util.AuthResponse;
import retrofit2.Call;
import retrofit2.http.*;

import java.util.List;

public interface ApiService {
    //Prod
    String BASE_URL = "http://api.all4dev.xyz/";

    //Dev
    //String BASE_URL = "http://127.0.0.1:8080/all4dev-api/";

    @POST("auth")
    @FormUrlEncoded
    Call<AuthResponse> auth(@Field("username") String username,
                            @Field("password") String password);

    //////////////////////////////////////////
    //        Me
    //////////////////////////////////////////

    @GET("me")
    Call<User> getMe(@Header("Authorization") String authorization);

    @GET("me/projects")
    Call<List<Project>> getProjects(@Header("Authorization") String authorization);



    //////////////////////////////////////////
    //        User
    //////////////////////////////////////////
    @GET("users")
    Call<List<User>> getUsers(
            @Header("Authorization") String authorization,
            @Query("username") String username,
            @Query("email") String email
    );

    @FormUrlEncoded
    @PUT("users/{username}")
    Call<User> putUser(
            @Header("Authorization") String authorization,
            @Path("username") String username,
            @Field("firstname") String firstname,
            @Field("lastname") String lastname,
            @Field("email") String email
    );

    @POST("users/{username}/keys")
    @Headers("Content-Type: application/json")
    Call<PublicKey> addPublicKey(@Header("Authorization") String authorization,
                                 @Path("username") String username,
                                 @Body PublicKey publicKey);



    //////////////////////////////////////////
    //        Project
    //////////////////////////////////////////
    @FormUrlEncoded
    @POST("users/{username}/projects")
    Call<Project> createProject(
            @Header("Authorization") String authorization,
            @Path("username") String username,
            @Field("name") String name,
            @Field("description") String description);


    @GET("users/{username}/projects/{projectname}/versions")
    Call<List<Version>> getProjectVersions(@Header("Authorization") String authorization,
                                           @Path("username") String username,
                                           @Path("projectname") String projectName);

    //////////////////////////////////////////
    //        Project
    //////////////////////////////////////////
    @GET("users/{username}/projects/{projectname}/members")
    Call<List<Member>> getProjectMembers(@Header("Authorization") String authorization,
                                         @Path("username") String username,
                                         @Path("projectname") String projectName);

    @FormUrlEncoded
    @POST("users/{username}/projects/{projectname}/members")
    Call<Member> addProjectMember(@Header("Authorization") String authorization,
                                  @Path("username") String username,
                                  @Path("projectname") String projectName,
                                  @Field("username") String memberUsername,
                                  @Field("access") String access);

    @FormUrlEncoded
    @PUT("users/{username}/projects/{projectname}/members/{memberUsername}")
    Call<Member> updateProjectMember(@Header("Authorization") String authorization,
                                     @Path("username") String username,
                                     @Path("projectname") String projectName,
                                     @Path("memberUsername") String memberUsername,
                                     @Field("access") String access);

    @DELETE("users/{username}/projects/{projectname}/members/{memberUsername}")
    Call<Void> removeProjectMember(@Header("Authorization") String authorization,
                                   @Path("username") String username,
                                   @Path("projectname") String projectName,
                                   @Path("memberUsername") String memberUsername);


    //////////////////////////////////////////
    //        Task
    //////////////////////////////////////////
    @GET("users/{username}/projects/{projectname}/tasks")
    Call<List<Task>> getTasks(@Header("Authorization") String authorization,
                              @Path("username") String username,
                              @Path("projectname") String projectName);


    @FormUrlEncoded
    @POST("users/{username}/projects/{projectname}/tasks")
    Call<Task> createTask(
            @Header("Authorization") String authorization,
            @Path("username") String username,
            @Path("projectname") String projectName,
            @Field("name") String name,
            @Field("type") String type,
            @Field("version") String vesion,
            @Field("body") String body);

    @GET("users/{username}/projects/{projectname}/tasks/{task_id}")
    Call<Task> getTask(@Header("Authorization") String authorization,
                       @Path("username") String username,
                       @Path("projectname") String projectName,
                       @Path("task_id") long taskId);


    //////////////////////////////////////////
    //        Task Status
    //////////////////////////////////////////
    @GET("users/{username}/projects/{projectname}/tasks/{task_id}/status")
    Call<List<TaskStatus>> getTaskStatus(@Header("Authorization") String authorization,
                                         @Path("username") String username,
                                         @Path("projectname") String projectName,
                                         @Path("task_id") long taskId);

    @FormUrlEncoded
    @PUT("users/{username}/projects/{projectname}/tasks/{task_id}/status")
    Call<TaskStatus> updateTaskStatus(@Header("Authorization") String authorization,
                                      @Path("username") String username,
                                      @Path("projectname") String projectName,
                                      @Path("task_id") long taskId,
                                      @Field("status") String status);



    //////////////////////////////////////////
    //        Task Members
    //////////////////////////////////////////
    @GET("users/{username}/projects/{projectname}/tasks/{task_id}/members")
    Call<List<TaskMember>> getTaskMembers(@Header("Authorization") String authorization,
                                          @Path("username") String username,
                                          @Path("projectname") String projectName,
                                          @Path("task_id") long taskId);


    @FormUrlEncoded
    @POST("users/{username}/projects/{projectname}/tasks/{task_id}/members")
    Call<TaskMember> addTaskMember(@Header("Authorization") String authorization,
                                   @Path("username") String username,
                                   @Path("projectname") String projectName,
                                   @Path("task_id") long taskId,
                                   @Field("username") String memberUsername);


    @DELETE("users/{username}/projects/{projectname}/tasks/{task_id}/members/{membername}")
    Call<Void> removeTaskMember(@Header("Authorization") String authorization,
                                @Path("username") String username,
                                @Path("projectname") String projectName,
                                @Path("task_id") long taskId,
                                @Path("membername") String memberUsername);



    //////////////////////////////////////////
    //        Task Comment
    //////////////////////////////////////////
    @GET("users/{username}/projects/{projectname}/tasks/{task_id}/comments")
    Call<List<Comment>> getTaskComments(@Header("Authorization") String authorization,
                                        @Path("username") String username,
                                        @Path("projectname") String projectName,
                                        @Path("task_id") long taskId);


    @FormUrlEncoded
    @POST("users/{username}/projects/{projectname}/tasks/{task_id}/comments")
    Call<Comment> addTaskComment(@Header("Authorization") String authorization,
                                 @Path("username") String username,
                                 @Path("projectname") String projectName,
                                 @Path("task_id") long taskId,
                                 @Field("body") String body);



    //////////////////////////////////////////
    //        Task Commit
    //////////////////////////////////////////
    @GET("users/{username}/projects/{projectname}/tasks/{task_id}/commits")
    Call<List<Commit>> getTaskCommits(@Header("Authorization") String authorization,
                                      @Path("username") String username,
                                      @Path("projectname") String projectName,
                                      @Path("task_id") long taskId);

    //////////////////////////////////////////
    //        Project Version
    //////////////////////////////////////////
    @FormUrlEncoded
    @POST("users/{username}/projects/{projectname}/versions")
    Call<Version> addVersion(@Header("Authorization") String authorization,
                                   @Path("username") String username,
                                   @Path("projectname") String projectName,
                                   @Field("version") String version);

    @FormUrlEncoded
    @PATCH("users/{username}/projects/{projectname}/versions")
    Call<Version> patchProjectVersion(@Header("Authorization") String authorization,
                                      @Path("username") String username,
                                      @Path("projectname") String projectName,
                                      @Field("version") String version);

    @DELETE("users/{username}/projects/{projectname}/versions/{version}")
    Call<Void> removeVersion(@Header("Authorization") String authorization,
                             @Path("username") String username,
                             @Path("projectname") String projectName,
                             @Path("version") String version);
}
