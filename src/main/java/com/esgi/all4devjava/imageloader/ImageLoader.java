package com.esgi.all4devjava.imageloader;

import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ImageLoader extends Thread {
    private static ImageLoader instance;

    private static final int BLOCKING_SIZE = 128;
    private BlockingQueue<ImageLoaderMessage> blockingQueue;

    private ImageLoader() {
        blockingQueue = new ArrayBlockingQueue<ImageLoaderMessage>(BLOCKING_SIZE);
    }

    public static boolean put(ImageView imageView, String url) {
        if (!isInstanced())
            throw new RuntimeException("ImageLoader Is Not Instanced");

        try {
            boolean found = false;
            for(ImageLoaderMessage message : instance.blockingQueue) {
                if (message.getImageView() == imageView) {
                    found = true;
                }
            }
            if (!found) {
                instance.blockingQueue.add(new ImageLoaderMessage(imageView, url));
            } else {
            }
            return !found;
        } catch (IllegalStateException e) {
            return false;
        }
    }

    private static boolean isInstanced() {
        return instance != null;
    }

    public static void init() {
        if (isInstanced())
            throw new RuntimeException("ImageLoader Already Instanced");

        instance = new ImageLoader();
        instance.start();
    }

    public static boolean interrupted() {
        if (!isInstanced())
            throw new RuntimeException("ImageLoader Is Not Instanced");
        instance.interrupt();
        return true;
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                ImageLoaderMessage message = blockingQueue.take();
                Image image = new Image(message.getURL());
                Platform.runLater(() -> {
                    message.getImageView().setImage(image);
                });
            } catch (InterruptedException e) {
                instance.blockingQueue.clear();
                instance = null;
                break;
            }
        }
    }
}
