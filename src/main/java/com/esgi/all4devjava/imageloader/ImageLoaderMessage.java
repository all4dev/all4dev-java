package com.esgi.all4devjava.imageloader;

import javafx.scene.image.ImageView;

public class ImageLoaderMessage {
    private ImageView imageView;
    private String url;

    public ImageLoaderMessage(ImageView imageView, String url) {
        this.imageView = imageView;
        this.url = url;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public String getURL() {
        return url;
    }
}
