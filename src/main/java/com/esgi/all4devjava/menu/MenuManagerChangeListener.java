package com.esgi.all4devjava.menu;

import javafx.scene.layout.Region;

public interface MenuManagerChangeListener {
    void onChangeListener(MenuItem item);
}
