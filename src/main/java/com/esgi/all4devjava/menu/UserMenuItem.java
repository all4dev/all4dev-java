package com.esgi.all4devjava.menu;

import com.esgi.all4devjava.controller.ProfileController;
import com.esgi.all4devjava.controller.TaskController;
import com.esgi.all4devjava.object.Project;
import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.object.User;

public class UserMenuItem extends MenuItem<ProfileController> {
    private String authKey;
    private User user;

    public UserMenuItem(String authKey, User user) {
        super(user.getUsername(), ProfileController.class);
        this.authKey = authKey;
        this.user = user;
    }

    @Override
    public void notify(ProfileController controller) {
        controller.notify(authKey, user);
    }

}
