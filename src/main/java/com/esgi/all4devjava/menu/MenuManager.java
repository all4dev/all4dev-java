package com.esgi.all4devjava.menu;

import com.esgi.all4devjava.controller.FragmentController;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.Notification;
import com.esgi.all4devjava.object.SessionData;
import com.esgi.all4devjava.object.Task;
import com.esgi.all4devjava.spamapi.SpamRequestApi;
import com.esgi.all4devjava.util.Util;
import javafx.css.PseudoClass;
import javafx.scene.control.Alert;
import javafx.scene.control.TreeCell;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class MenuManager implements Observer {

    private TreeView<MenuItem> treeView;
    private TreeItem<MenuItem> rootItem;

    private MenuManagerChangeListener changeListener;

    public MenuManager(TreeView<MenuItem> root, MenuManagerChangeListener changeListener) {
        SessionData.getInstance().addObserver(this);
        this.treeView = root;
        this.changeListener = changeListener;
        rootItem = new TreeItem<MenuItem>(new MenuItem<FragmentController>("", null) {
            @Override
            public void notify(FragmentController controller) {}
        });

        this.treeView.setShowRoot(false);
        this.treeView.setRoot(rootItem);

        this.treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null)
                load(newValue.getValue());
        });

        PseudoClass subElementPseudoClass = PseudoClass.getPseudoClass("sub-tree-item");

        this.treeView.setCellFactory(tv -> {
            TreeCell<MenuItem> cell = new TreeCell<MenuItem>() {
                @Override
                public void updateItem(MenuItem item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText("");
                    } else {
                        setText(item.getName());
                    }
                }
            };
            cell.treeItemProperty().addListener((obs, oldTreeItem, newTreeItem) -> {
                cell.pseudoClassStateChanged(subElementPseudoClass,
                        newTreeItem != null && newTreeItem.getParent() != cell.getTreeView().getRoot());
            });
            return cell;
        });
    }

    public TreeView<MenuItem> getTreeView() {
        return treeView;
    }

    public TreeItem<MenuItem> addItem(MenuItem menuItem) {
        TreeItem<MenuItem> treeItem = new TreeItem<MenuItem>(menuItem);
        rootItem.getChildren().add(treeItem);
        return treeItem;
    }

    public <T extends FragmentController> T getItem(Class<T> clazz) {
        for (TreeItem<MenuItem> item : rootItem.getChildren()) {
            if (item.getValue().getControllerClass().equals(clazz))
                return (T) item.getValue().getController();
        }
        return null;
    }

    public void forceClick(TreeItem<MenuItem> treeItem) {
        treeView.getSelectionModel().select(treeItem);
    }

    public void forceLoad(MenuItem item) {
        load(item);
        treeView.getSelectionModel().clearSelection();
    }

    public void load(MenuItem item) {
        SpamRequestApi spamRequestApi = new SpamRequestApi();
        spamRequestApi.requestCurrentTasks();
        //spamRequestApi.requestCurrentMembers();
        System.out.println("Selected Text : " + item.getName());
        if (item.getControllerClass() == null) {
            //No Content
            return;
        }
        if (item.getController() == null) {
            try {
                System.out.println("load controller for menu item " + item.getName());
                FragmentController controller = null;
                controller = (FragmentController) Util.loadController(treeView, item.getControllerClass());
                item.setController(controller);
                item.notify(controller);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        changeListener.onChangeListener(item);
    }

    @Override
    public void update(Observable o, Object arg) {
            if (arg instanceof ArrayList) {
                ArrayList list = (ArrayList) arg;
                if (list.get(0) instanceof Task) {
                    Notification notif = new Notification();
                    notif.setType("Task");
                    notif.setBody("Une ou plusieurs tâches viennent d'être ajoutées à votre projet ");
                    SessionData.getInstance().appendNotification(notif);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Nouvelle Notification");
                    alert.setContentText("Il y a de nouvelles notifications");


                    alert.showAndWait();
                }
                if (list.get(0) instanceof Member) {
                    Notification notif = new Notification();
                    notif.setType("Member");
                    notif.setBody("Un ou plusieurs membres viennent d'être ajoutées à votre projet ");
                    SessionData.getInstance().appendNotification(notif);
                }
            }
        System.out.println("update(" + o + "," + arg + ");");
    }
}
