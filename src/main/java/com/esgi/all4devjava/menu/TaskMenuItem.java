package com.esgi.all4devjava.menu;

import com.esgi.all4devjava.controller.RootController;
import com.esgi.all4devjava.controller.TaskController;
import com.esgi.all4devjava.object.Project;
import com.esgi.all4devjava.object.Task;

public class TaskMenuItem extends MenuItem<TaskController> {
    private RootController rootController;
    private String authKey;
    private Project project;
    private Task task;

    public TaskMenuItem(RootController rootController, String authKey, Project project, Task task) {
        super(task.getName(), TaskController.class);
        this.rootController = rootController;
        this.authKey = authKey;
        this.project = project;
        this.task = task;
    }

    @Override
    public void notify(TaskController controller) {
        controller.notify(rootController, authKey, project, task);
    }

    public Task getTask() {
        return task;
    }
}
