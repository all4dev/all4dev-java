package com.esgi.all4devjava.menu;

import com.esgi.all4devjava.controller.FragmentController;

public abstract class MenuItem<T extends FragmentController> {
    private String name;
    private Class<T> controllerClass;
    private FragmentController controller;

    public MenuItem(String name) {
        this.name = name;
    }

    public MenuItem(String name, Class<T> controllerClass) {
        this.name = name;
        this.controllerClass = controllerClass;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class<? extends FragmentController> getControllerClass() {
        return controllerClass;
    }

    public FragmentController getController() {
        return controller;
    }

    public void setController(FragmentController controller) {
        this.controller = controller;
    }

    public abstract void notify(T controller);
}
