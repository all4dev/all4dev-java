package com.esgi.all4devjava.object;

import com.esgi.all4devjava.util.DateTimeable;
import com.google.gson.annotations.SerializedName;
import javafx.scene.paint.Color;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public class Task implements DateTimeable {
    private long id;
    private String name;

    @SerializedName("branch_name")
    private String branchName;

    private String body;

    private Task.Type type;
    private Task.Status status;

    @SerializedName("author_id")
    private long authorId;

    @SerializedName("author_name")
    private String authorName;

    private String version;

    @SerializedName("last_update")
    private DateTime lastUpdate;


    private transient List<Commit> commits;

    private transient List<Comment> comments;

    private transient List<TaskMember> members;

    private transient List<TaskStatus> statuses;

    public Task() {
        commits = new ArrayList<Commit>();
        comments = new ArrayList<Comment>();
        members = new ArrayList<TaskMember>();
        statuses = new ArrayList<TaskStatus>();
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Task.Type getType() {
        return type;
    }

    public String getVersion() {
        return version;
    }

    public Task.Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public long getAuthorId() {
        return authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public DateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(DateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getRef() {
        return getType().toString().toLowerCase() + "/" + getBranchName();
    }

    public List<Commit> getCommits() {
        return commits;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public List<TaskMember> getMembers() {
        return members;
    }

    public List<TaskStatus> getStatuses() {
        return statuses;
    }

    @Override
    public DateTime getDateTime() {
        return getLastUpdate();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Task)) return false;

        Task task = (Task) o;

        return id == task.id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    public enum Type {
        TASK, BUG;
    }


    public enum Status {
        PENDING(Color.web("#A1A9A8"), "Attente"),
        RUNNING(Color.web("#A1A9A8"), "En cours"),
        FINISHED(Color.web("#A1A9A8"), "Fini"),
        MERGING(Color.web("#A1A9A8"), "En merge"),
        MERGED(Color.web("#A1A9A8"), "Merge");

        private Color color;
        private String display;

        Status(Color color, String display) {
            this.color = color;
            this.display = display;
        }

        public Color getColor() {
            return color;
        }

        public String getDisplay() {
            return display;
        }
    }
}
