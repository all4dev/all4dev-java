package com.esgi.all4devjava.object;

import javafx.scene.control.Alert;

import java.util.ArrayList;
import java.util.Observable;

/**
 * Created by meryl on 03/09/2016.
 */
public class SessionData extends Observable {

    private static final SessionData INSTANCE = new SessionData();
    private Project currentProject;
    private ArrayList<Task> currentTasks = new ArrayList<>();
    private ArrayList<Notification> currentNotifications = new ArrayList<>();
    private String authToken;
    private User currentUser;
    private ArrayList<Member> currentMembers = new ArrayList<>();



    public static SessionData getInstance() {
        return INSTANCE;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public Project getCurrentProject() {
        return currentProject;
    }

    public void setCurrentProject(Project currentProject) {
        this.currentProject = currentProject;
    }

    public ArrayList<Task> getCurrentTasks() {
        return currentTasks;
    }

    public void setCurrentTasks(ArrayList<Task> currentTasks) {
        if(this.currentTasks.size() == 0){
            this.currentTasks = currentTasks;
        }else {
            this.currentTasks = currentTasks;
            setChanged();
            notifyObservers(this.currentTasks);
        }
    }


    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public ArrayList<Notification> getCurrentNotifications() {
        return currentNotifications;
    }

    public void appendNotification(Notification notification){
        this.currentNotifications.add(notification);
    }

    public void removeNotification(int position){
        this.currentNotifications.remove(position);
    }

    public ArrayList<Member> getCurrentMembers() {
        return currentMembers;
    }

    public void setCurrentMembers(ArrayList<Member> currentMembers) {
        this.currentMembers = currentMembers;
        setChanged();
        notifyObservers(this.currentMembers);
    }

}
