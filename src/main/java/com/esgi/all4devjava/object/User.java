package com.esgi.all4devjava.object;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

public class User {
    private long id;
    private String username;
    private String email;
    private String firstname;
    private String lastname;

    @SerializedName("photo_path")
    private String photoPath;

    @SerializedName("creation_date")
    private DateTime creationDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public String getUrlPhotoPath() {
        return "http://res.all4dev.xyz/users/images/" + (photoPath == null ? "default.png" : photoPath);
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        return id == ((User) o).id;
    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", id=" + id +
                '}';
    }
}
