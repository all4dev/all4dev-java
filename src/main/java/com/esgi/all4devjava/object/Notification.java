package com.esgi.all4devjava.object;

/**
 * Created by meryl on 04/09/2016.
 */
public class Notification {

    private String type;
    private String body;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
