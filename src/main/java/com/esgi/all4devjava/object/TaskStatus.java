package com.esgi.all4devjava.object;

import com.esgi.all4devjava.util.DateTimeable;
import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

public class TaskStatus implements DateTimeable {
    private long id;
    private Task.Status status;

    @SerializedName("creation_date")
    protected DateTime creationDate;

    public Task.Status getStatus() {
        return status;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    @Override
    public DateTime getDateTime() {
        return getCreationDate();
    }
}
