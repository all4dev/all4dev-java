package com.esgi.all4devjava.object;

import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

public class TaskMember extends Member {

    @SerializedName("assign_date")
    private DateTime assignDate;

    @SerializedName("unassign_date")
    private DateTime unassignDate;

    public DateTime getAssignDate() {
        return assignDate;
    }

    public DateTime getUnassignDate() {
        return unassignDate;
    }
}
