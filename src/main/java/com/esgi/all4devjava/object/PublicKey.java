package com.esgi.all4devjava.object;

public class PublicKey {
    private String keyName;
    private String key;

    public PublicKey(String keyName, String key) {
        this.keyName = keyName;
        this.key = key;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}