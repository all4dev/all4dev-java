package com.esgi.all4devjava.object;

import com.esgi.all4devjava.util.DateTimeable;
import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

public class Commit implements DateTimeable{
    private String hash;
    private String subject;

    @SerializedName("author_name")
    private String authorName;

    @SerializedName("author_email")
    private String authorEmail;
    private DateTime date;

    @SerializedName("push_uuid")
    private String pushUUID;

    public String getHash() {
        return hash;
    }

    public String getSubject() {
        return subject;
    }

    public String getAuthorName() {
        return authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public DateTime getDate() {
        return date;
    }

    public String getPushUUID() {
        return pushUUID;
    }

    @Override
    public DateTime getDateTime() {
        return getDate();
    }
}
