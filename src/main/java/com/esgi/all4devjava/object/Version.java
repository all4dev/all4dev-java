package com.esgi.all4devjava.object;

/**
 * Created by tayeb on 04/09/2016.
 */
public class Version {
    private long id;
    private String version;

    public long getId() {
        return id;
    }

    public String getVersion() {
        return version;
    }
}
