package com.esgi.all4devjava.object;

import com.esgi.all4devjava.util.DateTimeable;
import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

public class Comment implements DateTimeable{

    private long id;
    private String body;

    @SerializedName("creation_date")
    private DateTime creationDate;

    @SerializedName("users_id")
    private long authorId;

    @SerializedName("author_name")
    private String authorName;

    public long getId() {
        return id;
    }

    public String getBody() {
        return body;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public long getAuthorId() {
        return authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    @Override
    public DateTime getDateTime() {
        return getCreationDate();
    }
}
