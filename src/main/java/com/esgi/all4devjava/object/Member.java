package com.esgi.all4devjava.object;

import com.google.gson.annotations.SerializedName;

public class Member extends User {
    private Member.Access access;

    @SerializedName("add_date")
    private String addDate;

    @SerializedName("remove_date")
    private String removeDate;

    public Member.Access getAccess() {
        return access;
    }

    public String getAddDate() {
        return addDate;
    }

    public String getRemoveDate() {
        return removeDate;
    }

    @Override
    public String toString() {
        return "Member{" +
                "username='" + getUsername() + '\'' +
                ", id=" + getId() +
                '}';
    }

    public enum Access {
        ADMIN, MOD, DEV, VIEWER;

        @Override
        public String toString() {
            String name = super.toString();
            return Character.toUpperCase(name.charAt(0)) + name.substring(1);
        }
    }
}
