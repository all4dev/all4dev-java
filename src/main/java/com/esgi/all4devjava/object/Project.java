package com.esgi.all4devjava.object;

import com.esgi.all4devjava.util.DateTimeable;
import com.esgi.all4devjava.util.GitManager;
import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;

import java.util.List;

public class Project implements DateTimeable {
    private long id;
    private String name;
    private String description;

    @SerializedName("repo_name")
    private String repoName;

    @SerializedName("creation_date")
    private DateTime creationDate;

    @SerializedName("leader_id")
    private long leaderId;

    @SerializedName("leader_username")
    private String leaderUsername;

    @SerializedName("last_update")
    private DateTime lastUpdate;

    private transient List<Member> members;

    private String version;

    private List<String> versions;


    private transient List<Task> tasks;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRepoName() {
        return repoName;
    }

    public DateTime getCreationDate() {
        return creationDate;
    }

    public long getLeaderId() {
        return leaderId;
    }

    public String getLeaderUsername() {
        return leaderUsername;
    }

    public DateTime getLastUpdate() {
        return lastUpdate;
    }

    public List<Member> getMembers() {
        return members;
    }

    public void setMembers(List<Member> members) {
        this.members = members;
    }

    public Member getMember(User user) {
        for (Member member : getMembers()) {
            if (user.equals(member))
                return member;
        }
        return null;
    }

    public String getCurrentVersion() {
        return version;
    }

    public void setCurrentVersion(String n) {
        this.version = n;
    }

    public List<String> getVersions() {
        return versions;
    }

    public void setVersions(List<String> versions) {
        this.versions = versions;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    @Override
    public DateTime getDateTime() {
        return getLastUpdate();
    }

    public String getGitUri() {
        return GitManager.BASE_URI + getLeaderUsername() + "/" + getRepoName();
    }
}
