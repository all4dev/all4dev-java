package com.esgi.all4devjava.util;

import org.eclipse.jgit.api.GitCommand;
import org.eclipse.jgit.api.errors.GitAPIException;

public class GitManagerCommand<T> {
    private String title;
    private GitCommand<T> gitCommand;

    public GitManagerCommand(String title, GitCommand<T> gitCommand) {
        this.title = title;
        this.gitCommand = gitCommand;
    }

    public String getTitle() {
        return title;
    }

    public GitCommand<T> getGitCommand() {
        return gitCommand;
    }

    public void onResult(T t) {}
}
