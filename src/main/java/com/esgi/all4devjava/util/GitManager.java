package com.esgi.all4devjava.util;

import com.esgi.all4devjava.object.Project;
import com.jcraft.jsch.Session;
import javafx.application.Platform;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.SshTransport;
import org.eclipse.jgit.transport.Transport;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class GitManager extends Thread {

    //Prod
    public static final String BASE_URI = "git@all4dev.xyz:/";

    //Dev
    //public static final String BASE_URI = "ssh://git@127.0.0.1:2222/";

    private static final int BLOCKING_SIZE = 5;
    private BlockingQueue<GitManagerCommands> blockingQueue;
    private GitManager.Listener listener;

    private Project project;
    private File folder;

    private Git git;
    private final Object gitLock = new Object();

    public GitManager() {
        blockingQueue = new ArrayBlockingQueue<GitManagerCommands>(BLOCKING_SIZE);
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public void setFolder(File folder) {
        this.folder = folder;
    }

    public void setPrivateKey(String privateKeyPath) {
        SshSessionFactory.setInstance(new A4DSshSessionFactory(privateKeyPath) {
            @Override
            protected void configure(OpenSshConfig.Host host, Session session) {
                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                session.setConfig(config);
            }
        });
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public synchronized Git getGit() {
        synchronized (gitLock) {
            return git;
        }
    }

    public synchronized void setGit(Git git) {
        synchronized (gitLock) {
            this.git = git;
        }
    }

    public boolean put(GitManagerCommands gitManagerCommands) {
        try {
            blockingQueue.add(gitManagerCommands);
            return true;
        } catch (IllegalStateException e) {
            return false;
        }
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                GitManagerCommands commands = blockingQueue.take();
                for (int i = 0; i < commands.getCommands().size(); i++) {
                    GitManagerCommand command = commands.getCommands().get(i);
                    if (command == null) {
                        continue;
                    }
                    System.out.println("Run  :" + command.getTitle());
                    if (listener != null) {
                        listener.onCall(command);
                    }
                    try {
                        Object obj = command.getGitCommand().call();
                        if (obj instanceof Git)
                            setGit((Git) obj);
                        int finalI = i + 1;
                        Platform.runLater(() -> {
                            command.onResult(obj);
                            if (finalI == commands.getCommands().size()) {
                                commands.onFinish();
                            }
                        });
                    } catch (Exception e) {
                        System.out.println("Error w/ " + command.getTitle());
                        e.printStackTrace();
                        if (e instanceof GitAPIException) {
                            Platform.runLater(() -> commands.onError((GitAPIException) e));
                        }
                        break;
                    }
                }
            } catch (InterruptedException e) {
                blockingQueue.clear();
                break;
            }
        }
    }

    public void loadOrClone() {
        if (isEmptyFolder(folder)) {
            put(new GitManagerCommands(
                    new GitManagerCommand<Git>("Clone du projet", Git.cloneRepository().setURI(project.getGitUri())
                            .setDirectory(folder))
            ));
        } else {
            try {
                setGit(Git.open(folder));
                System.out.println("Git " + folder.getAbsolutePath() + " Loaded");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isEmptyFolder(File folder) {
        if (!folder.isDirectory())
            throw new RuntimeException(folder.getAbsolutePath() + " is not a directory");

        return folder.list().length == 0;
    }

    public interface Listener {
        void onCall(GitManagerCommand command);
    }

    public void finish() {
        interrupt();
        blockingQueue.clear();
        if (git != null)
            git.close();
    }
}
