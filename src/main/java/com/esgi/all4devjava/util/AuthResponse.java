package com.esgi.all4devjava.util;

import com.google.gson.annotations.SerializedName;

public class AuthResponse {

    @SerializedName("Authorization")
    private String key;

    public String getKey() {
        return key;
    }
}
