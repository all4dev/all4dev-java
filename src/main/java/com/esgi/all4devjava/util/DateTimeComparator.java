package com.esgi.all4devjava.util;

import java.util.Comparator;

public class DateTimeComparator<T extends DateTimeable> implements Comparator<T> {

    DateTimeComparator.Sort sort;

    public DateTimeComparator() {
        this(DateTimeComparator.Sort.ASC);
    }

    public DateTimeComparator(DateTimeComparator.Sort sort) {
        this.sort = sort;
    }

    @Override
    public int compare(T o1, T o2) {
        return (o1.getDateTime().getMillis() < o2.getDateTime().getMillis() ? 1 : -1) * (sort == DateTimeComparator.Sort.ASC ? -1 : 1);
    }

    public enum Sort {
        ASC, DESC
    }
}
