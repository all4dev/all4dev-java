package com.esgi.all4devjava.util;

import org.joda.time.DateTime;

public interface DateTimeable {
    DateTime getDateTime();
}
