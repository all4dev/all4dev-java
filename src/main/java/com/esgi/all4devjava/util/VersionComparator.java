package com.esgi.all4devjava.util;

import java.util.Comparator;

/**
 * Created by tayeb on 27/08/2016.
 */
public class VersionComparator implements Comparator<String> {
    @Override
    public int compare(String o1, String o2) {
        return Util.compareVersion(o1, o2);
    }
}
