package com.esgi.all4devjava.util;

import java.io.File;

public class StorageManager {
    public static String ROOT_DIR_PATH = null;

    private static String getA4DDirPath() {
        return (ROOT_DIR_PATH == null) ? System.getProperty("user.home") + File.separator  + "All4Dev" : ROOT_DIR_PATH;
    }

    public static File getA4DDir() {
        return new File(getA4DDirPath());
    }

    public static String getSettingFilePath() {
        return getA4DDirPath() + File.separator + "settings.json";
    }

    public static String getKnownHostsFilePath() {
        return getA4DDirPath() + File.separator + "knownhosts";
    }

    private static String getProjectsDirPath() {
        return getA4DDirPath() + File.separator + "Projects";
    }

    private static File getProjectsDir() {
        return new File(getProjectsDirPath());
    }

    private static String getProjectDirPath(String name) {
        return getProjectsDirPath() + File.separator + name;
    }

    public static File getProjectDir(String name) {
        return new File(getProjectDirPath(name));
    }
}
