package com.esgi.all4devjava.util;


import com.google.gson.Gson;

import java.io.*;

public class Settings {
    private String privateKeyPath;
    private String publicKeyPath;

    public static Settings load() throws IOException {
        FileReader fileReader = new FileReader(StorageManager.getSettingFilePath());
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        Gson gson = new Gson();
        String json = "";
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            json+=line;
        }
        Settings settings = gson.fromJson(json, Settings.class);
        bufferedReader.close();
        fileReader.close();
        return settings;
    }

    public void save() throws IOException {
        FileWriter file = new FileWriter(StorageManager.getSettingFilePath());
        file.write(new Gson().toJson(this));
        file.flush();
        file.close();
    }

    public String getPrivateKeyPath() {
        return privateKeyPath;
    }

    public void setPrivateKeyPath(String privateKeyPath) {
        this.privateKeyPath = privateKeyPath;
    }

    public String getPublicKeyPath() {
        return publicKeyPath;
    }

    public void setPublicKeyPath(String publicKeyPath) {
        this.publicKeyPath = publicKeyPath;
    }
}
