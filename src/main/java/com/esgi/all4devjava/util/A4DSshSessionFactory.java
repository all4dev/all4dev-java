package com.esgi.all4devjava.util;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.util.FS;

import java.io.File;
import java.io.IOException;

public class A4DSshSessionFactory extends JschConfigSessionFactory {
    private String path;

    public A4DSshSessionFactory(String path) {
        this.path = path;
    }

    @Override
    protected JSch createDefaultJSch(FS fs) throws JSchException {
        JSch defaultJSch = super.createDefaultJSch(fs);
        defaultJSch.addIdentity(path);
        File file = new File(StorageManager.getKnownHostsFilePath());
        if (!file.exists())
            try {
                file.createNewFile();
                defaultJSch.setKnownHosts(StorageManager.getKnownHostsFilePath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        return defaultJSch;
    }

    @Override
    protected void configure(OpenSshConfig.Host host, Session session) {

    }
}
