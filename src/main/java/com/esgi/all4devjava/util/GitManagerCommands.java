package com.esgi.all4devjava.util;

import org.eclipse.jgit.api.errors.GitAPIException;

import java.util.ArrayList;
import java.util.List;

public class GitManagerCommands {

    public List<GitManagerCommand> commands;

    public GitManagerCommands(GitManagerCommand... commands) {
        this.commands = new ArrayList<>(commands.length);
        for( GitManagerCommand command : commands)
            this.commands.add(command);
    }

    public void onError(GitAPIException e) {}

    public void onFinish() {}

    public List<GitManagerCommand> getCommands() {
        return commands;
    }
}
