package com.esgi.all4devjava.util.gsonadapter;

import com.esgi.all4devjava.object.Member;
import com.google.gson.*;

import java.lang.reflect.Type;

public class EnumAdapter<T extends Enum<T>> implements JsonSerializer<T>, JsonDeserializer<T> {

    Class<T> anEnum;

    public EnumAdapter(Class<T> anEnum) {
        this.anEnum = anEnum;
    }

    @Override
    public T deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return jsonElement.isJsonNull() ? null : T.valueOf(anEnum, jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(T t, Type type, JsonSerializationContext jsonSerializationContext) {
        return t == null ? null : new JsonPrimitive(t.toString());
    }
}
