package com.esgi.all4devjava.util.gsonadapter;

import com.google.gson.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

public class DateTimeAdapter implements JsonSerializer<DateTime>, JsonDeserializer<DateTime> {
    private DateTimeFormatter formatter;

    public DateTimeAdapter() {
        this("yyyy-MM-dd'T'HH:mm:ssZ");
    }

    public DateTimeAdapter(String format) {
        this.formatter = DateTimeFormat.forPattern(format);
    }

    @Override
    public DateTime deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return jsonElement.isJsonNull() ? null : formatter.parseDateTime(jsonElement.getAsString());
    }

    @Override
    public JsonElement serialize(DateTime date, Type type, JsonSerializationContext jsonSerializationContext) {
        return date == null ? new JsonNull() : new JsonPrimitive(formatter.print(date));
    }
}
