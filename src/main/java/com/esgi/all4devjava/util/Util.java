package com.esgi.all4devjava.util;

import com.esgi.all4devjava.MainApp;
import com.esgi.all4devjava.controller.Controller;
import com.esgi.all4devjava.controller.dialog.DialogController;
import com.esgi.all4devjava.menu.MenuItem;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.User;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Labeled;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.SegmentedButton;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Util {

    public static final class ContextItem {
        public static final javafx.scene.control.MenuItem UPDATE = new javafx.scene.control.MenuItem("Modifier");
        public static final javafx.scene.control.MenuItem DELETE = new javafx.scene.control.MenuItem("Supprimer");
    }

    public static URL getResourceLayout(String name) {
        return getResource("layout", name);
    }

    public static URL getResourceItem(String name) {
        return getResource("item", name);
    }

    private static URL getResource(String dir, String file) {
        return MainApp.class.getResource("/view/" + dir + "/" + file + ".fxml");
    }

    public static <T extends Controller> T loadController(Node node, Class<T> controllerClass) throws NoSuchFieldException, IllegalAccessException, IOException {
        return loadController((Stage) node.getScene().getWindow(), controllerClass);
    }

    public static <T extends Controller> T loadController(Stage stage, Class<T> controllerClass) throws NoSuchFieldException, IllegalAccessException, IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setControllerFactory((Class<?> type) -> {
            try {
                Object newController = type.newInstance();
                if (newController instanceof Controller) {
                    ((Controller) newController).setPrimaryStage(stage);
                }
                return newController;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        URL layoutURL = (URL) controllerClass.getField("layoutURL").get(null);
        if (layoutURL == null)
            throw new RuntimeException(controllerClass.getCanonicalName() + " not redefined public static layoutURL");

        loader.setLocation(layoutURL);
        Node root = loader.load();
        if (loader.getController() == null)
            throw new RuntimeException("No controller was set in the layoutURL of " + controllerClass.getCanonicalName() + "(" + layoutURL.getPath() + ")");

        String loaderControllerClassName = loader.getController().getClass().getCanonicalName();
        if (!loader.getController().getClass().equals(controllerClass))
            throw new RuntimeException("The Controller set in the layoutURL of " + loaderControllerClassName +  "(" + layoutURL.getPath() + ") is not himself");

        if (!(loader.getController() instanceof Controller))
            throw new RuntimeException(loaderControllerClassName + " not extended to Controller");


        T controller = loader.getController();
        controller.setRoot((Region) root);
        return controller;
    }

    public static final <T extends DialogController> T loadDialog(Stage stage, Class<T> controllerClass) throws NoSuchFieldException, IllegalAccessException, IOException {
        Controller controller = Util.loadController(stage, controllerClass);
        if (!(controller instanceof DialogController))
            throw new RuntimeException(controller.getClass().getCanonicalName() + " not extended to Controller");

        DialogController dialogController = (DialogController) controller;
        dialogController.initDialog();

        return (T) dialogController;
    }

    public static final String getUserPhotoURL(User user) {
        return "http://res.all4dev.xyz/users/images/" + (user.getPhotoPath() == null ? "default.png" : user.getPhotoPath());
    }

    public static final void setTooltip(Labeled labeled, String body) {
        setTooltip(labeled, body, 150);
    }

    public static final void setTooltip(Labeled labeled, String body, int duration) {
        Tooltip tooltip = new Tooltip(body);
        hackTooltipStartTiming(tooltip, duration);
        //labeled.setTextFill(Color.web("#0066cc"));
        //labeled.setCursor(Cursor.HAND);
        labeled.setTooltip(tooltip);
    }

    public static final void hackTooltipStartTiming(Tooltip tooltip, int duration) {
        try {
            Field fieldBehavior = tooltip.getClass().getDeclaredField("BEHAVIOR");
            fieldBehavior.setAccessible(true);
            Object objBehavior = fieldBehavior.get(tooltip);

            Field fieldTimer = objBehavior.getClass().getDeclaredField("activationTimer");
            fieldTimer.setAccessible(true);
            Timeline objTimer = (Timeline) fieldTimer.get(objBehavior);

            objTimer.getKeyFrames().clear();
            objTimer.getKeyFrames().add(new KeyFrame(new Duration(duration)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDateFormated(DateTime date) {
        int days = Util.diffDay(date);
        if (days == 0)
            return "Aujourd\'hui à " + dateToString("HH:mm", date);
        else if (days == 1)
            return "Hier à " + dateToString("HH:mm", date);
        else if (days < 7) {
            return dateToString("EEEE 'à' HH:mm", date);
        }
        return dateToString("dd MMM yyyy 'à' HH:mm", date);
    }

    public static String dateToString(String format, DateTime date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
        return date.toString(formatter);
    }

    public static DateTime getNow() {
        return new DateTime().withZone(DateTimeZone.getDefault());
    }

    public static int diffDay(DateTime date) {
        try {
            return Days.daysBetween(date.withZone(DateTimeZone.getDefault()).withTimeAtStartOfDay(), Util.getNow().withTimeAtStartOfDay()).getDays();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static <T extends Enum> ToggleButton[] getToggleFromEnum(Class<T> anEnum) {
        List<ToggleButton> toggleButtons = new ArrayList<>();
        for(T t : anEnum.getEnumConstants()) {
            ToggleButton toggleButton = new ToggleButton(t.toString());
            toggleButton.setUserData(t);
            toggleButtons.add(toggleButton);
        }
        return toggleButtons.toArray(new ToggleButton[toggleButtons.size()]);
    }


    public static <T> void toggleUserData(SegmentedButton segmentedButton, T userData) {
        for (ToggleButton button : segmentedButton.getButtons()) {
            Object btnUserData = button.getUserData();
            System.out.println(btnUserData.toString());
            System.out.println(userData.toString());
            if (btnUserData == userData) {
                System.out.println("OK");
                segmentedButton.getToggleGroup().selectToggle(button);
                break;
            }
        }
    }

    public static int compareVersion(String v1, String v2) {
        String[] vals1 = v1.split("\\.");
        String[] vals2 = v2.split("\\.");
        int i = 0;
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }
        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
            return Integer.signum(diff);
        }
        return Integer.signum(vals1.length - vals2.length);

    }

    public static boolean versionIsBelowOrEqual(String version, String currentVersion) {
        return Util.compareVersion(version, currentVersion) == -1 || version.equalsIgnoreCase(currentVersion);
    }
}
