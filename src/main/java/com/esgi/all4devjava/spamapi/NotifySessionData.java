package com.esgi.all4devjava.spamapi;

import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.SessionData;
import com.esgi.all4devjava.object.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by meryl on 04/09/2016.
 */
public class NotifySessionData {

    public static void sendTasks(List<Task> tasks){
        ArrayList<Task> updatedList = (ArrayList<Task>) tasks;
        SessionData.getInstance().setCurrentTasks(updatedList);
    }
    public static void sendMembers(List<Member> members){
        ArrayList<Member> updatedList = (ArrayList<Member>) members;
        SessionData.getInstance().setCurrentMembers(updatedList);
    }

}
