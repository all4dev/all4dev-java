package com.esgi.all4devjava.spamapi;

import com.esgi.all4devjava.api.ApiManager;
import com.esgi.all4devjava.api.ApiRequest;
import com.esgi.all4devjava.api.ApiService;
import com.esgi.all4devjava.exception.A4DException;
import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.SessionData;
import com.esgi.all4devjava.object.Task;
import retrofit2.Call;
import retrofit2.Response;

import java.util.List;

/**
 * Created by meryl on 03/09/2016.
 */
public class SpamRequestApi {

    private ApiManager apiManager;
    private List<Task> taskList;
    private List<Member> memberList;


    public void requestCurrentTasks(){
        apiManager = new ApiManager() {
            @Override
            public void onPreExec() {

            }

            @Override
            public void onSuccess() {
                if(!taskList.isEmpty()){
                    if(!SessionData.getInstance().getCurrentTasks().isEmpty()) {

                        if (!ComparatorSessionData.tasksComparator(taskList)) {
                            NotifySessionData.sendTasks(taskList);
                        }
                    } else {
                        NotifySessionData.sendTasks(taskList);
                    }
                }
            }

            @Override
            public void onError(A4DException e) {

            }

            @Override
            public void always() {


            }
        };
        apiManager.execute(new ApiRequest<List<Task>>() {
            @Override
            public Call<List<Task>> call(ApiService service) {
                return service.getTasks(SessionData.getInstance().getAuthToken(),SessionData.getInstance().getCurrentProject().getLeaderUsername(),SessionData.getInstance().getCurrentProject().getName());
            }

            @Override
            public void onResponse(Call<List<Task>> call, Response<List<Task>> response) {
                taskList = response.body();
            }
        });
    }
    public void requestCurrentMembers(){
        apiManager = new ApiManager() {
            @Override
            public void onPreExec() {

            }

            @Override
            public void onSuccess() {
                if(!memberList.isEmpty()){
                    if(!SessionData.getInstance().getCurrentMembers().isEmpty()) {

                        if (!ComparatorSessionData.membersComparator(memberList)) {
                            NotifySessionData.sendMembers(memberList);
                        }
                    } else {
                        NotifySessionData.sendMembers(memberList);
                    }
                }
            }

            @Override
            public void onError(A4DException e) {

            }

            @Override
            public void always() {


            }
        };
        apiManager.execute(new ApiRequest<List<Member>>() {
            @Override
            public Call<List<Member>> call(ApiService service) {
                return service.getProjectMembers(SessionData.getInstance().getAuthToken(),SessionData.getInstance().getCurrentProject().getLeaderUsername(),SessionData.getInstance().getCurrentProject().getName());
            }

            @Override
            public void onResponse(Call<List<Member>> call, Response<List<Member>> response) {
                memberList = response.body();
            }
        });
    }

}
