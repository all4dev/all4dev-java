package com.esgi.all4devjava.spamapi;

import com.esgi.all4devjava.object.Member;
import com.esgi.all4devjava.object.SessionData;
import com.esgi.all4devjava.object.Task;

import java.util.List;


public class ComparatorSessionData {

    public static boolean tasksComparator(List<Task> tasks) {
        if(tasks.size() != SessionData.getInstance().getCurrentTasks().size()) {
            return false;
        } else {
            for (int i = 0;i < tasks.size();i++){
                if(tasks.get(i).equals(SessionData.getInstance().getCurrentTasks().get(i))){
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }

    }

    public static boolean membersComparator(List<Member> members) {
        if(members.size() != SessionData.getInstance().getCurrentMembers().size()) {
            return false;
        } else {
            for (int i = 0;i < members.size();i++){
                if(members.get(i).equals(SessionData.getInstance().getCurrentMembers().get(i))){
                    continue;
                } else {
                    return false;
                }
            }
            return true;
        }

    }

}
